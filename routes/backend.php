<?php

use App\Http\Controllers\Backend\BannerController;
use App\Http\Controllers\Backend\BannernoticeController;
use App\Http\Controllers\Backend\CareercategoryController;
use App\Http\Controllers\Backend\CareerController;
use App\Http\Controllers\Backend\CareerpositionController;
use App\Http\Controllers\Backend\Contact\ContactedController;
use App\Http\Controllers\Backend\Contact\ReportinformationController;
use App\Http\Controllers\Backend\ContactusController;
use App\Http\Controllers\Backend\DepartmentController;
use App\Http\Controllers\Backend\DesignationController;
use App\Http\Controllers\Backend\GalleryController;
use App\Http\Controllers\Backend\ImportantlinkController;
use App\Http\Controllers\Backend\NewsController;
use App\Http\Controllers\Backend\NoticecategoryController;
use App\Http\Controllers\Backend\NoticeController;
use App\Http\Controllers\Backend\PopupnoticeController;
use App\Http\Controllers\Backend\RegulationcategoryController;
use App\Http\Controllers\Backend\RegulationController;
use App\Http\Controllers\Backend\RolesAndPermission\TrainingdepartmentController;
use App\Http\Controllers\Backend\RolesAndPermission\TrainingroleController;
use App\Http\Controllers\Backend\RolesAndPermission\TrainingstaffController;
use App\Http\Controllers\Backend\StaffController;
use App\Http\Controllers\Backend\TendercategoryController;
use App\Http\Controllers\Backend\TenderController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::group(['middleware' => 'admin'], function () {
    Route::get('dashboard', function () {
        return view('dashboard.index');
    })->name('dashboard.index');
    Route::resource('admin-banner-notice', BannernoticeController::class);
    Route::resource('admin-banner', BannerController::class);
    Route::resource('admin-notice-category', NoticecategoryController::class);
    Route::resource('admin-notice', NoticeController::class);
    Route::resource('admin-news', NewsController::class);
    Route::resource('admin-tender-category', TendercategoryController::class);
    Route::resource('admin-tender', TenderController::class);
    Route::resource('admin-career-category', CareercategoryController::class);
    Route::resource('admin-career-position', CareerpositionController::class);
    Route::resource('admin-career', CareerController::class);
    Route::resource('admin-designation', DesignationController::class);
    Route::resource('admin-department', DepartmentController::class);
    Route::resource('admin-staff', StaffController::class);
    Route::resource('admin-gallery', GalleryController::class);
    Route::get('admin/galleries/image/remove/{galleryimage}', 'App\Http\Controllers\Backend\GalleryController@removeGalleryImage')->name('admin.galleryimage.remove');

    Route::resource('admin-important-link', ImportantlinkController::class);
    Route::resource('admin-contact-detail', ContactusController::class);
    Route::resource('admin-trainer', TrainingstaffController::class);
    Route::resource('admin-training-role', TrainingroleController::class);
    Route::resource('admin-training-department', TrainingdepartmentController::class);
    Route::resource('admin-regulation-category', RegulationcategoryController::class);
    Route::resource('admin-regulation', RegulationController::class);

    Route::resource('admin-pop-notice', PopupnoticeController::class);

    Route::resource('admin-contacted', ContactedController::class);
    Route::resource('admin-report-information', ReportinformationController::class);

    Route::get('settings', 'App\Http\Controllers\Backend\Configuration\ConfigurationController@getConfiguration')->name('settings');
    Route::post('settings', 'App\Http\Controllers\Backend\Configuration\ConfigurationController@postConfiguration')->name('settings.update');
    Route::get('admin-about-us', 'App\Http\Controllers\Backend\Configuration\AboutusController@getConfiguration')->name('admin-about-us');
    Route::post('admin-about-us', 'App\Http\Controllers\Backend\Configuration\AboutusController@postConfiguration')->name('admin-about-us.update');
});

Route::get('/admin/login', 'App\Http\Controllers\Admin\Auth\AdminloginController@adminLogin')->name('admin/login');
Route::post('/loginadmin', 'App\Http\Controllers\Admin\Auth\AdminloginController@postAdminLogin')->name('loginadmin');
Route::get('/adminlogout', 'App\Http\Controllers\Admin\Auth\AdminloginController@adminLogout')->name('adminlogout');
