<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', 'App\Http\Controllers\Frontend\Home\HomeController@index')->name('/');
Route::get('about-us', 'App\Http\Controllers\Frontend\AboutUs\AboutusController@index')->name('about-us');
Route::get('tender', 'App\Http\Controllers\Frontend\Tender\TenderController@index')->name('tender');
Route::get('career', 'App\Http\Controllers\Frontend\Career\CareerController@index')->name('career');
Route::get('notice', 'App\Http\Controllers\Frontend\Notice\NoticeController@index')->name('notice');
Route::get('contact-us', 'App\Http\Controllers\Frontend\ContactUs\ContactusController@index')->name('contact-us');

Route::post('contact-us/post', 'App\Http\Controllers\Frontend\ContactUs\ContactusController@postIndex')->name('contact-us.post');

Route::get('gallery', 'App\Http\Controllers\Frontend\Gallery\GalleryController@index')->name('gallery');

Route::get('report-information', 'App\Http\Controllers\Frontend\ReportInformation\ReportInformationController@index')->name('report-information');
Route::post('report-information/post', 'App\Http\Controllers\Frontend\ReportInformation\ReportInformationController@postIndex')->name('report-information.post');

Route::get('regulation', 'App\Http\Controllers\Frontend\Regulation\RegulationController@index')->name('regulation');
Route::get('news-detail/{slug}', 'App\Http\Controllers\Frontend\Home\HomeController@getNewsdetail')->name('news-detail');
