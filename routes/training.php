<?php

use App\Http\Controllers\Training\Dashboard\Material\MaterialController;
use App\Http\Controllers\Training\Dashboard\Trainee\TraineeController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Training\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::group(['middleware' => 'training'], function () {
    Route::get('training-dashboard', function () {
        return view('Training.dashboard.index');
    })->name('training-dashboard.index');

    Route::resource('training-trainee', TraineeController::class);
    Route::resource('training-material', MaterialController::class);
});
// Route::get('/training/dashboard', 'App\Http\Controllers\Training\Dashboard\Home\HomeController@index')->name('training-dashboard');

// Route::get('/training/login', 'App\Http\Controllers\Training\Auth\LoginController@trainingLogin')->name('training/login');
// Route::post('/logintraining', 'App\Http\Controllers\Training\Auth\LoginController@postTrainingLogin')->name('logintraining');
// Route::get('/traininglogout', 'App\Http\Controllers\Training\Auth\LoginController@trainingLogout')->name('traininglogout');
// Route::get('/training/loginone', 'App\Http\Controllers\Training\Auth\LoginController@trainingLoginone')->name('training/loginone');
// Route::get('/training/logintwo', 'App\Http\Controllers\Training\Auth\LoginController@trainingLogintwo')->name('training/logintwo');

Route::get('/training/page', [LoginController::class, 'getTraining'])->name('training.page');

Route::get('/training/login', [LoginController::class, 'showEmailForm'])->name('training.login.email');
Route::post('/training/login', [LoginController::class, 'submitEmail'])->name('training.login.email.submit');
Route::get('/training/login-password', [LoginController::class, 'showPasswordForm'])->name('training.login.password');
Route::post('/training/login-password', [LoginController::class, 'submitPassword'])->name('training.login.password.submit');

Route::get('/traininglogout', [LoginController::class, 'trainingLogout'])->name('traininglogout');
