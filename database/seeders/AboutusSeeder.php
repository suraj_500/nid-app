<?php

namespace Database\Seeders;

use App\Models\Configuration\AboutUs;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AboutusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
         //First About Us
         $configuration=new AboutUs();
         $configuration->configuration_key='first_title';
         $configuration->configuration_value='Welcome To Nepal Investigation Department';
         $configuration->save();

         $configuration=new AboutUs();
         $configuration->configuration_key='first_description';
         $configuration->configuration_value='';
         $configuration->save();

         $configuration=new AboutUs();
         $configuration->configuration_key='first_title_np';
         $configuration->configuration_value='नेपाल अनुसन्धान विभागमा स्वागत छ';
         $configuration->save();

         $configuration=new AboutUs();
         $configuration->configuration_key='first_description_np';
         $configuration->configuration_value='';
         $configuration->save();

         $configuration=new AboutUs();
         $configuration->configuration_key='image_one';
         $configuration->configuration_value='';
         $configuration->save();

         $configuration=new AboutUs();
         $configuration->configuration_key='image_two';
         $configuration->configuration_value='';
         $configuration->save();

         $configuration=new AboutUs();
         $configuration->configuration_key='image_three';
         $configuration->configuration_value='';
         $configuration->save();

         //Second About Us
         $configuration=new AboutUs();
         $configuration->configuration_key='second_title';
         $configuration->configuration_value='Welcome To Nepal Investigation Department';
         $configuration->save();

         $configuration=new AboutUs();
         $configuration->configuration_key='second_description';
         $configuration->configuration_value='';
         $configuration->save();

         $configuration=new AboutUs();
         $configuration->configuration_key='second_title_np';
         $configuration->configuration_value='नेपाल अनुसन्धान विभागमा स्वागत छ';
         $configuration->save();

         $configuration=new AboutUs();
         $configuration->configuration_key='second_description_np';
         $configuration->configuration_value='';
         $configuration->save();

         $configuration=new AboutUs();
         $configuration->configuration_key='image_four';
         $configuration->configuration_value='';
         $configuration->save();

         $configuration=new AboutUs();
         $configuration->configuration_key='image_five';
         $configuration->configuration_value='';
         $configuration->save();

         $configuration=new AboutUs();
         $configuration->configuration_key='image_six';
         $configuration->configuration_value='';
         $configuration->save();

         //mission statement
         $configuration=new AboutUs();
         $configuration->configuration_key='mission_title';
         $configuration->configuration_value='Our Mission Statement';
         $configuration->save();

         $configuration=new AboutUs();
         $configuration->configuration_key='mission_description';
         $configuration->configuration_value='Mission statement description';
         $configuration->save();

         $configuration=new AboutUs();
         $configuration->configuration_key='mission_title_np';
         $configuration->configuration_value='हाम्रो मिशन कथन';
         $configuration->save();

         $configuration=new AboutUs();
         $configuration->configuration_key='mission_description_np';
         $configuration->configuration_value='मिशन कथन विवरण';
         $configuration->save();
    }
}
