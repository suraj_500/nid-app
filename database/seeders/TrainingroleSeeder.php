<?php

namespace Database\Seeders;

use App\Models\RolesAndPermission\TrainingRole;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TrainingroleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        TrainingRole::insert([
        	'training_role'=>'Trainer',
            'training_role_np'=>'प्रशिक्षक',
        	'status'=>'1',
        ]);

        TrainingRole::insert([
        	'training_role'=>'Trainee',
            'training_role_np'=>'प्रशिक्षार्थी',
        	'status'=>'1',
        ]);
    }
}
