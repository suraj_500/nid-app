<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Auth\Admin;


class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Admin::insert([
        	'first_name'=>'NID',
            'last_name'=>'Admin',
        	'email'=>'nid@gmail.com',
        	'password'=>bcrypt('admin12345'),
        ]);
    }
}
