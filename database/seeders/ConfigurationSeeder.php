<?php

namespace Database\Seeders;

use App\Models\Configuration\Configuration;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ConfigurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
         //Maps
         $configuration=new Configuration();
         $configuration->configuration_key='map';
         $configuration->configuration_value='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7718.315131179322!2d85.31858004111626!3d27.698094729231222!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb19a52cce15cb%3A0x23ba5de75230f29a!2sNational%20Investigation%20Department!5e0!3m2!1sen!2snp!4v1718021795013!5m2!1sen!2snp';
         $configuration->save();

         $configuration=new Configuration();
         $configuration->configuration_key='copyright';
         $configuration->configuration_value='';
         $configuration->save();

         //Icons
         $configuration=new Configuration();
         $configuration->configuration_key='logo';
         $configuration->configuration_value='';
         $configuration->save();

         $configuration=new Configuration();
         $configuration->configuration_key='fab_icon';
         $configuration->configuration_value='';
         $configuration->save();

         $configuration=new Configuration();
         $configuration->configuration_key='footer_logo';
         $configuration->configuration_value='';
         $configuration->save();

         $configuration=new Configuration();
         $configuration->configuration_key='nav_title';
         $configuration->configuration_value='Title About NID on Nav bar';
         $configuration->save();

         $configuration=new Configuration();
         $configuration->configuration_key='fab_title';
         $configuration->configuration_value='Title for fab icon';
         $configuration->save();

         //Contact english
         $configuration=new Configuration();
         $configuration->configuration_key='contact_address';
         $configuration->configuration_value='National Research Department Singhadarwar, Kathmandu';
         $configuration->save();

         $configuration=new Configuration();
         $configuration->configuration_key='contact_email_primary';
         $configuration->configuration_value='nidsecretariat@nidept.gov.np';
         $configuration->save();

         $configuration=new Configuration();
         $configuration->configuration_key='contact_phone_primary';
         $configuration->configuration_value='01-4211559';
         $configuration->save();

         $configuration=new Configuration();
         $configuration->configuration_key='fax';
         $configuration->configuration_value='+977-01-4211689';
         $configuration->save();

         $configuration=new Configuration();
         $configuration->configuration_key='contact_email_secondary';
         $configuration->configuration_value='infoict@nidept.gov.np';
         $configuration->save();

         $configuration=new Configuration();
         $configuration->configuration_key='contact_phone_secondary';
         $configuration->configuration_value='01-4211572';
         $configuration->save();

         //Contact Nepali
         $configuration=new Configuration();
         $configuration->configuration_key='contact_address_np';
         $configuration->configuration_value='राष्ट्रिय अनुसन्धान विभाग सिंहदरवार, काठमाडौं';
         $configuration->save();

         $configuration=new Configuration();
         $configuration->configuration_key='contact_email_primary_np';
         $configuration->configuration_value='nidsecretariat@nidept.gov.np';
         $configuration->save();

         $configuration=new Configuration();
         $configuration->configuration_key='contact_phone_primary_np';
         $configuration->configuration_value='०१-४२१११५५९';
         $configuration->save();

         $configuration=new Configuration();
         $configuration->configuration_key='fax_np';
         $configuration->configuration_value='+९७७-०१-४२१११६८९';
         $configuration->save();

         $configuration=new Configuration();
         $configuration->configuration_key='contact_email_secondary_np';
         $configuration->configuration_value='infoict@nidept.gov.np';
         $configuration->save();

         $configuration=new Configuration();
         $configuration->configuration_key='contact_phone_secondary_np';
         $configuration->configuration_value='०१-४२११५७२';
         $configuration->save();
    }
}
