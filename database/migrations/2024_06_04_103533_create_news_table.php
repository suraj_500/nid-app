<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('title_np')->nullable();
            $table->text('description')->nullable();
            $table->text('description_np')->nullable();
            $table->string('image')->nullable();
            $table->string('slug')->nullable();
            $table->string('published_date')->nullable();
            $table->string('author')->nullable();
            $table->string('author_np')->nullable();
            $table->string('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('news');
    }
};
