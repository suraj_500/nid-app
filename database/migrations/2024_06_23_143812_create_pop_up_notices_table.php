<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pop_up_notices', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('title_np')->nullable();
            $table->string('image')->nullable();
            $table->string('slug')->nullable();
            $table->string('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pop_up_notices');
    }
};
