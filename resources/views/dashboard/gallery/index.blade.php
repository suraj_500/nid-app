@extends('dashboard.layouts.app')
@push('css')
<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endpush

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header mt-5">
                    <h3 class="card-title">Gallery</h3>
                    <span>
                        <a href="{{ route('admin-gallery.create') }}" class="btn btn-sm btn-success float-right">Add New</a>
                    </span>
                </div>
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped yajra-datatable">
                        <thead>
                            <tr>
                                <th>S.N.</th>
                                <th>Full Title</th>
                                <th>पुरा शीर्षक</th>
                                <th>Description</th>
                                <th>विवरण</th>
                                <th>Status</th>
                                <th>View</th>
                                <th width="150px">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@foreach($galleries as $gallery)
<!-- Modal -->
<div class="modal fade" id="previewGalleryImages{{$gallery->id}}" tabindex="-1" role="dialog" aria-labelledby="previewGalleryImages{{$gallery->id}}" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center">{{$gallery->title}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    @forelse($gallery->galleryImages as $galleryImage)
                    <div class="col-md-3 col-sm-4 col-lg-3 m-2">
                        <img src="{{ asset('storage/' . $galleryImage->image) }}" style="height: 120px; width:120px;" alt="{{$gallery->title}}"><br>
                        <a href="{{route('admin-gallery.index', ['galleryimage'=>$galleryImage->id])}}" class="danger">Remove</a>
                    </div>
                    @empty
                    <p class="modal-title text-center">Images unavailable.</p>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach

<div class="modal fade" id="confirmDeleteModal">
    <div class="modal-dialog">
        <form method="POST" id="deleteGalleryForm" action="">
            @csrf
            @method('DELETE')
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Delete Confirmation</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="text-center text-bold">Are you sure you want to delete this?</p>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No, Go Back</button>
                    <button type="submit" class="btn btn-danger">Yes, Delete</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@push('js')
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(function() {
        var table = $('.yajra-datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin-gallery.index') }}",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'title', name: 'title'},
                {data: 'title_np', name: 'title_np'},
                {data: 'description', name: 'description'},
                {data: 'description_np', name: 'description_np'},
                {data: 'status', name: 'status'},
                {data: 'view', name: 'view', orderable: false, searchable: false},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
            columnDefs: [
                { targets: 6, className: 'dt-body-center' },
                { targets: 7, className: 'dt-body-center' }
            ]
        });
    });

    function handleDelete(id) {
        var form = document.getElementById('deleteGalleryForm');
        form.action = '/admin-gallery/' + id;
        $('#confirmDeleteModal').modal('show');
    }
</script>
@endpush
