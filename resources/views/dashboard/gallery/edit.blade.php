@extends('dashboard.layouts.app')
@push('css')
<link rel="stylesheet" href="{{ asset('backend/assets/summernote/summernote-bs4.min.css') }}">

@endpush
@section('content')
<div class="page-content">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                    <h4 class="mb-sm-0 font-size-18">Gallery</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Gallery</a></li>
                            <li class="breadcrumb-item active">Create</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <form action="{{route('admin-gallery.update',$data->id)}}" method="post" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="card-body">

                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Full Title</label>
                                <div class="col-md-3">
                                    <input class="form-control" name="title" type="text" value="{{$data->title}}" id="example-text-input">
                                </div>
                                <label for="example-search-input" class="col-md-2 col-form-label">पुरा शीर्षक</label>
                                <div class="col-md-3">
                                    <input class="form-control" name="title_np" type="text" value="{{$data->title_np}}" id="nepali">
                                </div>
                            </div>

                            <div class="mb-3 row">
                                <label for="example-email-input" class="col-md-2 col-form-label">Description</label>
                                <div class="col-md-12">
                                    <textarea class="summernote form-control" name="description" id="description">{!!$data->description!!}</textarea>
                                </div>
                                <label for="example-email-input" class="col-md-2 col-form-label">विवरण</label>
                                <div class="col-md-12">
                                    <textarea class="summernote_two form-control" name="description_np" id="description_np">{!!$data->description_np!!}</textarea>
                                </div>
                            </div>

                            <div class="mb-3 row">
                                <label for="example-email-input" class="col-md-2 col-form-label">File</label>
                                <div class="col-md-3">
                                    <input class="form-control" name="image[]" type="file" value="" placeholder="" id="galleryimages" multiple> <br>
                                    <div id="previews"></div>
                                </div>

                                @if(isset($data))
                                @if(count($data->galleryimages) > 0)
                                <label for="current-image">Current Images</label>
                                <div class="row">
                                    @foreach($data->galleryimages as $galleryimage)
                                    <div class="col-md-3 col-sm-4 col-lx-3">
                                        <img src="{{asset($galleryimage->image) }}" style="height: 60px; width:60px;" alt="{{$data->title}}"><br>
                                        <a href="{{route('admin.galleryimage.remove', ['galleryimage'=>$galleryimage->id])}}">Remove</a>
                                    </div>
                                    @endforeach
                                </div>
                                @else
                                <p class="text-center">No images available.</p>
                                @endif
                                @endif

                            </div>


                        </div>

                        <div class="card-body">
                            <div class="col-sm-6">
                                <div class="mt-4">
                                    <div>
                                        <div class="input-group">
                                            <button class="btn btn-primary" type="submit" id="inputGroupFileAddon04">Update</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div> <!-- end col -->
        </div>
        <!-- end row -->

    </div> <!-- container-fluid -->
</div>
<!-- End Page-content -->
@endsection
@push('js')
<script src="https://unpkg.com/nepalify"></script>
<script>
    nepalify.interceptElementById("nepali");
</script>
<!-- jQuery -->
<script src="{{ asset('backend/assets/summernote/summernote-bs4.min.js') }}"></script>
<script>
    $(function() {
        // Initialize Summernote for the first editor with the "Italic" tool
        $('.summernote').summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['link', 'picture', 'video']],
                ['misc', ['codeview', 'fullscreen']]
            ],
            tabDisable: false // Enable tab key functionality
        });

        // Initialize CodeMirror for the first textarea
        CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
            mode: "htmlmixed",
            theme: "monokai"
        });
    });

    $(function() {
        // Initialize Summernote for the second editor with the "Italic" tool
        $('.summernote_two').summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['link', 'picture', 'video']],
                ['misc', ['codeview', 'fullscreen']]
            ],
            tabDisable: false // Enable tab key functionality
        });

        // Initialize CodeMirror for the second textarea
        CodeMirror.fromTextArea(document.getElementById("codeMirrorDemoTwo"), {
            mode: "htmlmixed",
            theme: "monokai"
        });
    });
</script>
<script>
    $("#galleryimages").change(function(e) {
        var files = e.originalEvent.srcElement.files;
        var imgPreview = document.getElementById('previews');
        if (files) {
            $('#previews').html("");
            var fileslength = files.length;
            for (i = 0; i < fileslength; i++) {
                if (files[i].type == 'image/png' || files[i].type == 'image/jpg' || files[i].type == 'image/jpeg') {
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        $($.parseHTML("<img class='pic' style='height: 90px; width:90px;'>"))
                            .attr('src', event.target.result).appendTo(imgPreview);
                    }
                    reader.readAsDataURL(files[i]);
                }
            }
        }
    });
</script>
@endpush
