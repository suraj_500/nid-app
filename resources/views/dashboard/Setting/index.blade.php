@extends('dashboard.layouts.app')
@push('css')
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
@endpush
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>
                    Page Settings
                </h1>
            </div>

            <!-- {{-- @include('../backend.admin.config.notify') --}} -->

        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <form action="{{ route('settings.update') }}" method="post" class="form-horizontal"
            enctype="multipart/form-data">
            {{ csrf_field() }}
            <!-- /.row -->
            <div class="card card-primary card-outline">
                <div class="card-body">
                    <ul class="nav nav-tabs" id="custom-content-above-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="custom-content-above-home-tab" data-toggle="pill"
                                href="#custom-content-above-home" role="tab" aria-controls="custom-content-above-home"
                                aria-selected="true">Maps</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="custom-content-above-icons-tab" data-toggle="pill"
                                href="#custom-content-above-icons" role="tab"
                                aria-controls="custom-content-above-icons" aria-selected="false">Icons</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="custom-content-above-contactus-tab" data-toggle="pill"
                                href="#custom-content-above-contactus" role="tab"
                                aria-controls="custom-content-above-contactus" aria-selected="false">Contact Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="custom-content-above-footer-tab" data-toggle="pill"
                                href="#custom-content-above-footer" role="tab"
                                aria-controls="custom-content-above-footer" aria-selected="false">Contact Us (Nepali)</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="custom-content-above-tabContent">

                        <div class="tab-pane fade show active" id="custom-content-above-home" role="tabpanel"
                            aria-labelledby="custom-content-above-home-tab">
                            @include('dashboard.Setting.map.aboutus')
                        </div>
                        <div class="tab-pane fade" id="custom-content-above-icons" role="tabpanel"
                            aria-labelledby="custom-content-above-icons-tab">
                            @include('dashboard.Setting.Icon.icons')
                        </div>
                        <div class="tab-pane fade show" id="custom-content-above-contactus" role="tabpanel"
                            aria-labelledby="custom-content-above-contactus-tab">
                            @include('dashboard.Setting.contactus.contactus')
                        </div>
                        <div class="tab-pane fade show" id="custom-content-above-footer" role="tabpanel"
                            aria-labelledby="custom-content-above-footer-tab">
                            @include('dashboard.Setting.contactus_np.index')
                        </div>

                    </div>
                    <button type="submit" class="btn btn-info pull-right"
                        style="float:right; margin-right:15px">Update</button>
                </div>

                <!-- /.card -->
            </div>
            <!-- /.card -->

    </div>

    </form>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>



@endsection

@push('js')
<script>
    $(function () {
                $('.select2').select2({
                    placeholder: 'Select Options'
                });

                $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                    checkboxClass: 'icheckbox_minimal-red',
                    radioClass: 'iradio_minimal-red'
                });
            });
</script>
<script>
    var sitelogo = document.getElementById('selectedSiteLogo');
            sitelogo.style.display = "none";
            $("#site_logo").change(function (e) {
                var file = e.originalEvent.srcElement.files[0];
                if (file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg') {
                    var reader = new FileReader();
                    reader.onloadend = function () {
                        sitelogo.src = reader.result;
                    }
                    reader.readAsDataURL(file);
                    sitelogo.style.display = "block";
                    document.getElementById('selectSiteLogoInfo').innerHTML =
                        "<strong><font color='red'>Image name: </font></strong>" + file.name;
                } else {
                    document.getElementById('selectSiteLogoInfo').innerHTML =
                        "<strong><font color='red'>Error: </font></strong>" +
                        'Selected file is not image. Please select image';
                }
            });
            // for favicon
            var favicon = document.getElementById('selectedSiteFavicon');
            favicon.style.display = "none";
            $("#site_favicon").change(function (e) {
                var file = e.originalEvent.srcElement.files[0];
                if (file.type == 'image/x-icon' || file.type == 'image/png' || file.type == 'image/jpg') {
                    var reader = new FileReader();
                    reader.onloadend = function () {
                        favicon.src = reader.result;
                    }
                    reader.readAsDataURL(file);
                    favicon.style.display = "block";
                    document.getElementById('selectSiteFaviconInfo').innerHTML =
                        "<strong><font color='red'>Image name: </font></strong>" + file.name;
                } else {
                    document.getElementById('selectSiteFaviconInfo').innerHTML =
                        "<strong><font color='red'>Error: </font></strong>" +
                        'Selected file is not image. Please select image';
                }
            });
            // for Brochure
            var aboutpageimg = document.getElementById('selectedAboutPageImg');
            aboutpageimg.style.display = "none";
            $("#login_img").change(function (e) {
                var file = e.originalEvent.srcElement.files[0];
                if (file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg') {
                    var reader = new FileReader();
                    reader.onloadend = function () {
                        aboutpageimg.src = reader.result;
                    }
                    reader.readAsDataURL(file);
                    aboutpageimg.style.display = "block";
                    document.getElementById('selectedAboutPageImgInfo').innerHTML =
                        "<strong><font color='red'>Image name: </font></strong>" + file.name;
                }  else {
                    document.getElementById('selectedAboutPageImgInfo').innerHTML =
                        "<strong><font color='red'>Error: </font></strong>" +
                        'Selected file is not image. Please select image.';
                }
            });
</script>

<!-- for message notifications -->
<script type="text/javascript">
    $("document").ready(function()
  {
    setTimeout(function()
{
  $("div.alert").remove();
},3000);
  });
</script>

<!-- For success msg -->
<!-- <script src="{{asset('backend/plugins/toastr/toastr.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('backend/plugins/toastr/toastr.min.css')}}" />
@if(session()->has('success'))
<script>
    toastr.success("{!! session('success') !!}");
</script>
@endif -->
@endpush
