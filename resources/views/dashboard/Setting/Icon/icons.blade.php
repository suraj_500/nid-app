<div class="card card-primary">
    <!-- /.card-header -->
    <!-- form start -->
    <div class="card-body d-flex gap-2 flex-wrap">
        <div class="form-group">
            <label for="exampleInputEmail1">Logo:</label>
            <input type="file" name="logo" class="form-control" id="exampleInputEmail1"
                value="{{ getConfiguration('logo')}}">
            <img src="{{asset(getConfiguration('logo'))}}" width="200px" height="200px" style="object-fit: contain;">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Fab Icon:</label>
            <input type="file" name="fab_icon" class="form-control" id="exampleInputEmail1"
                value="{{ getConfiguration('fab_icon')}}">
            <img src="{{asset(getConfiguration('fab_icon'))}}" width="200px" height="200px"
                style="object-fit: contain;">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Footer Logo:</label>
            <input type="file" name="footer_logo" class="form-control" id="exampleInputEmail1"
                value="{{ getConfiguration('footer_logo')}}">
            <img src="{{asset(getConfiguration('footer_logo'))}}" width="200px" height="200px"
                style="object-fit: contain;">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Nav Title:</label>
            <input type="text" name="nav_title" class="form-control" id="exampleInputEmail1"
                value="{{ getConfiguration('nav_title')}}">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Fab Title:</label>
            <input type="text" name="fab_title" class="form-control" id="exampleInputEmail1"
                value="{{ getConfiguration('fab_title')}}">
        </div>
    </div>
</div>
