<div class="card card-primary">
    <!-- /.card-header -->
    <!-- form start -->
    <div class="card-body">
        <div class="form-group">
            <label for="exampleInputEmail1">Location:</label>
            <input type="text" name="contact_address" class="form-control" id="exampleInputEmail1"
                value="{{ getConfiguration('contact_address')}}">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Primary Email Address:</label>
            <input type="text" name="contact_email_primary" class="form-control" id="exampleInputEmail1"
                value="{{ getConfiguration('contact_email_primary')}}">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Primary Contact Number:</label>
            <input type="text" name="contact_phone_primary" class="form-control" id="exampleInputEmail1"
                value="{{ getConfiguration('contact_phone_primary')}}">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Fax:</label>
            <input type="text" name="fax" class="form-control" id="exampleInputEmail1"
                value="{{ getConfiguration('fax')}}">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Secondary Email Address:</label>
            <input type="text" name="contact_email_secondary" class="form-control" id="exampleInputEmail1"
                value="{{ getConfiguration('contact_email_secondary')}}">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Secondary Contact Number:</label>
            <input type="text" name="contact_phone_secondary" class="form-control" id="exampleInputEmail1"
                value="{{ getConfiguration('contact_phone_secondary')}}">
        </div>
    </div>
</div>
