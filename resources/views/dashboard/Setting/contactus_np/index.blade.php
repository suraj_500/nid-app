<div class="card card-primary">
    <!-- /.card-header -->
    <!-- form start -->
    <div class="card-body">
    <div class="form-group">
            <label for="exampleInputEmail1">स्थान:</label>
            <input type="text" name="contact_address_np" class="form-control" id="exampleInputEmail1"
                value="{{ getConfiguration('contact_address_np')}}">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">प्राथमिक इमेल ठेगाना:</label>
            <input type="text" name="contact_email_primary_np" class="form-control" id="exampleInputEmail1"
                value="{{ getConfiguration('contact_email_primary_np')}}">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">प्राथमिक सम्पर्क नम्बर:</label>
            <input type="text" name="contact_phone_primary_np" class="form-control" id="exampleInputEmail1"
                value="{{ getConfiguration('contact_phone_primary_np')}}">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">फ्याक्स:</label>
            <input type="text" name="fax_np" class="form-control" id="exampleInputEmail1"
                value="{{ getConfiguration('fax_np')}}">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">माध्यमिक इमेल ठेगाना:</label>
            <input type="text" name="contact_email_secondary_np" class="form-control" id="exampleInputEmail1"
                value="{{ getConfiguration('contact_email_secondary_np')}}">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">माध्यमिक सम्पर्क नम्बर:</label>
            <input type="text" name="contact_phone_secondary_np" class="form-control" id="exampleInputEmail1"
                value="{{ getConfiguration('contact_phone_secondary_np')}}">
        </div>
    </div>
</div>

