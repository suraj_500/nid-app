<div class="card card-primary">
    <!-- /.card-header -->
    <!-- form start -->
    <div class="card-body">
        <div class="form-group">
            <label for="exampleInputEmail1">Map:</label>
            <input type="text" name="map" class="form-control" id="exampleInputEmail1"
                value="{{ getConfiguration('map')}}">
                <iframe src="{{ getConfiguration('map')}}" title="Map" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Copy Right:</label>
            <input type="text" name="copyright" class="form-control" id="exampleInputEmail1"
                value="{{ getConfiguration('copyright')}}">
        </div>
    </div>
</div>
