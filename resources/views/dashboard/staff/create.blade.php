@extends('dashboard.layouts.app')
@push('css')
<link rel="stylesheet" href="{{ asset('backend/assets/summernote/summernote-bs4.min.css') }}">

@endpush
@section('content')
<div class="page-content">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                    <h4 class="mb-sm-0 font-size-18">Career</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Career</a></li>
                            <li class="breadcrumb-item active">Create</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <form action="{{route('admin-staff.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">

                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Full Title</label>
                                <div class="col-md-3">
                                    <input class="form-control" name="name" type="text" placeholder="Full Title" id="example-text-input">
                                </div>
                                <label for="example-search-input" class="col-md-2 col-form-label">पुरा शीर्षक</label>
                                <div class="col-md-3">
                                    <input class="form-control" name="name_np" type="text" placeholder="पुरा शीर्षक" id="nepali">
                                </div>
                            </div>

                            <div class="mb-3 row">
                                <label for="example-email-input" class="col-md-2 col-form-label">Description</label>
                                <div class="col-md-12">
                                    <textarea class="summernote form-control" name="description" id="description"></textarea>
                                </div>
                                <label for="example-email-input" class="col-md-2 col-form-label">विवरण</label>
                                <div class="col-md-12">
                                    <textarea class="summernote_two form-control" name="description_np" id="description_np"></textarea>
                                </div>
                            </div>

                            <div class="mb-3 row">
                                <label for="example-email-input" class="col-md-2 col-form-label">File</label>
                                <div class="col-md-3">
                                    <input class="form-control" name="image" type="file" value="" placeholder="" id="example-email-input">
                                </div>

                            </div>

                            <!-- <div class="mb-3 row">
                                <label for="example-search-input" class="col-md-2 col-form-label">Category</label>
                                <div class="col-md-3">
                                    <select name="category_id" class="form-control" id="example-search-input">
                                        @if($category)
                                        @foreach($category as $category)
                                        <option value="{{$category->id}}">{{$category->title}}
                                        </option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div> -->

                            <div class="mb-3 row">
                                <label for="example-search-input" class="col-md-2 col-form-label">Designation</label>
                                <div class="col-md-3">
                                    <select name="designation_id" class="form-control" id="example-search-input">
                                        @if($position)
                                        @foreach($position as $position)
                                        <option value="{{$position->id}}">{{$position->name}}
                                        </option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="col-sm-6">
                                <div class="mt-4">
                                    <div>
                                        <div class="input-group">
                                            <button class="btn btn-primary" type="submit" id="inputGroupFileAddon04">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div> <!-- end col -->
        </div>
        <!-- end row -->

    </div> <!-- container-fluid -->
</div>
<!-- End Page-content -->
@endsection
@push('js')
<script src="https://unpkg.com/nepalify"></script>
<script>
    nepalify.interceptElementById("nepali");
</script>
<!-- jQuery -->
<script src="{{ asset('backend/assets/summernote/summernote-bs4.min.js') }}"></script>
<script>
    $(function() {
        // Initialize Summernote for the first editor with the "Italic" tool
        $('.summernote').summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['link', 'picture', 'video']],
                ['misc', ['codeview', 'fullscreen']]
            ],
            tabDisable: false // Enable tab key functionality
        });

        // Initialize CodeMirror for the first textarea
        CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
            mode: "htmlmixed",
            theme: "monokai"
        });
    });

    $(function() {
        // Initialize Summernote for the second editor with the "Italic" tool
        $('.summernote_two').summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['link', 'picture', 'video']],
                ['misc', ['codeview', 'fullscreen']]
            ],
            tabDisable: false // Enable tab key functionality
        });

        // Initialize CodeMirror for the second textarea
        CodeMirror.fromTextArea(document.getElementById("codeMirrorDemoTwo"), {
            mode: "htmlmixed",
            theme: "monokai"
        });
    });
</script>
@endpush
