@extends('dashboard.layouts.app')

@section('content')
<div class="page-content">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                    <h4 class="mb-sm-0 font-size-18">Site Contact Details</h4>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Site Contact Details</a></li>
                            <li class="breadcrumb-item active">Create</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <form action="{{route('admin-contact-detail.update',$data->id)}}" method="post" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="card-body">
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Location</label>
                                <div class="col-md-12">
                                    <input class="form-control" name="location" type="text" value="{{$data->location}}" placeholder="Notice" id="example-text-input">
                                </div>
                                <label for="example-search-input" class="col-md-2 col-form-label">स्थान</label>
                                <div class="col-md-12">
                                    <input class="form-control" name="location_np" type="text" value="{{$data->location_np}}" placeholder="सूचना" id="nepali">
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Contact Number</label>
                                <div class="col-md-12">
                                    <input class="form-control" name="phone" type="text" value="{{$data->phone}}" placeholder="Notice" id="example-text-input">
                                </div>
                                <label for="example-search-input" class="col-md-2 col-form-label">सम्पर्क नम्बर</label>
                                <div class="col-md-12">
                                    <input class="form-control" name="phone_np" type="text" value="{{$data->phone_np}}" placeholder="सूचना" id="nepali_two">
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="col-sm-6">
                                <div class="mt-4">
                                    <div>
                                        <div class="input-group">
                                            <button class="btn btn-primary" type="submit" id="inputGroupFileAddon04">Update</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
    </div> <!-- container-fluid -->
</div> <!-- End Page-content -->
@endsection
@push('js')
<script src="https://unpkg.com/nepalify"></script>
<script>
    nepalify.interceptElementById("nepali");
    nepalify.interceptElementById("nepali_two");
</script>
@endpush
