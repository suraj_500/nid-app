<div class="card card-primary">
    <!-- /.card-header -->
    <!-- form start -->
    <div class="card-body">
        <div class="form-group">
            <label for="exampleInputEmail1">First Section Title:</label>
            <input type="text" name="second_title_np" class="form-control" id="exampleInputEmail1" value="{{ getAboutus('second_title_np')}}">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">First Section Description:</label>
            <input type="text" name="second_description_np" class="form-control" id="exampleInputEmail1" value="{{ getAboutus('second_description_np')}}">
        </div>
    </div>
</div>
