<div class="card card-primary">
    <!-- /.card-header -->
    <!-- form start -->
    <div class="card-body">
    <div class="form-group">
            <label for="exampleInputEmail1">Second Section Title:</label>
            <input type="text" name="second_title" class="form-control" id="exampleInputEmail1"
                value="{{ getAboutus('second_title')}}">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Second Section Description:</label>
            <input type="text" name="second_description" class="form-control" id="exampleInputEmail1"
                value="{{ getAboutus('second_description')}}">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">First Image:</label>
            <input type="file" name="image_four" class="form-control" id="exampleInputEmail1"
                value="{{ getAboutus('image_four')}}">
            <img src="{{asset(getAboutus('image_four'))}}" width="200px" height="200px"
                style="object-fit: contain;">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Second Image:</label>
            <input type="file" name="image_five" class="form-control" id="exampleInputEmail1"
                value="{{ getAboutus('image_five')}}">
            <img src="{{asset(getAboutus('image_five'))}}" width="200px" height="200px"
                style="object-fit: contain;">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Third Image:</label>
            <input type="file" name="image_six" class="form-control" id="exampleInputEmail1"
                value="{{ getAboutus('image_six')}}">
            <img src="{{asset(getAboutus('image_six'))}}" width="200px" height="200px"
                style="object-fit: contain;">
        </div>
    </div>
</div>
