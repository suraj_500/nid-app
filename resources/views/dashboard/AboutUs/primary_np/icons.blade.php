<div class="card card-primary">
    <!-- /.card-header -->
    <!-- form start -->
    <div class="card-body d-flex gap-2 flex-wrap">
        <div class="form-group">
            <label for="exampleInputEmail1">First Section Title:</label>
            <input type="text" name="first_title_np" class="form-control" id="exampleInputEmail1" value="{{ getAboutus('first_title_np')}}">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">First Section Description:</label>
            <input type="text" name="first_description_np" class="form-control" id="exampleInputEmail1" value="{{ getAboutus('first_description_np')}}">
        </div>
    </div>
</div>
