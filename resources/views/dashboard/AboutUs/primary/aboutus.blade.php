<div class="card card-primary">
    <div class="card-body">
        <div class="form-group">
            <label for="exampleInputEmail1">First Section Title:</label>
            <input type="text" name="first_title" class="form-control" id="exampleInputEmail1"
                value="{{ getAboutus('first_title')}}">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">First Section Description:</label>
            <input type="text" name="first_description" class="form-control" id="exampleInputEmail1"
                value="{{ getAboutus('first_description')}}">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">First Image:</label>
            <input type="file" name="image_one" class="form-control" id="exampleInputEmail1"
                value="{{ getAboutus('image_one')}}">
            <img src="{{asset(getAboutus('image_one'))}}" width="200px" height="200px"
                style="object-fit: contain;">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Second Image:</label>
            <input type="file" name="image_two" class="form-control" id="exampleInputEmail1"
                value="{{ getAboutus('image_two')}}">
            <img src="{{asset(getAboutus('image_two'))}}" width="200px" height="200px"
                style="object-fit: contain;">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Third Image:</label>
            <input type="file" name="image_three" class="form-control" id="exampleInputEmail1"
                value="{{ getAboutus('image_three')}}">
            <img src="{{asset(getAboutus('image_three'))}}" width="200px" height="200px"
                style="object-fit: contain;">
        </div>
    </div>
</div>
