<div class="card card-primary">
    <div class="card-body">
        <div class="form-group">
            <label for="exampleInputEmail1">Mission Statement Title:</label>
            <input type="text" name="mission_title" class="form-control" id="exampleInputEmail1"
                value="{{ getAboutus('mission_title')}}">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Mission Statement Description:</label>
            <input type="text" name="mission_description" class="form-control" id="exampleInputEmail1"
                value="{{ getAboutus('mission_description')}}">
        </div>
    </div>
    <div class="card-body">
        <div class="form-group">
            <label for="exampleInputEmail1">मिशन कथन शीर्षक</label>
            <input type="text" name="mission_title_np" class="form-control" id="exampleInputEmail1"
                value="{{ getAboutus('mission_title_np')}}">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">मिशन कथन विवरण:</label>
            <input type="text" name="mission_description_np" class="form-control" id="exampleInputEmail1"
                value="{{ getAboutus('mission_description_np')}}">
        </div>
    </div>
</div>
