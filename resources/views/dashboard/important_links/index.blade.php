@extends('dashboard.layouts.app')
@push('css')
<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endpush

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header mt-5">
                    <h3 class="card-title">Important Links</h3>
                    <span>
                        <a href="{{ route('admin-important-link.create') }}" class="btn btn-sm btn-success float-right">Add New</a>
                    </span>
                </div>
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped yajra-datatable">
                        <thead>
                            <tr>
                                <th>S.N.</th>
                                <th>Full Title</th>
                                <th>पुरा शीर्षक</th>
                                <th>URL/Link</th>
                                <th>Status</th>
                                <th width="150px">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="confirmDeleteModal">
    <div class="modal-dialog">
        <form method="POST" id="deleteGalleryForm" action="">
            @csrf
            @method('DELETE')
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Delete Confirmation</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="text-center text-bold">Are you sure you want to delete this?</p>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No, Go Back</button>
                    <button type="submit" class="btn btn-danger">Yes, Delete</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@push('js')
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(function() {
        var table = $('.yajra-datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin-important-link.index') }}",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'title', name: 'title'},
                {data: 'title_np', name: 'title_np'},
                {data: 'url', name: 'url'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
    });

    function handleDelete(id) {
        var form = document.getElementById('deleteGalleryForm');
        form.action = 'admin-important-link/' + id;
        $('#confirmDeleteModal').modal('show');
    }
</script>
@endpush
