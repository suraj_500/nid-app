<!--begin::Fonts(mandatory for all pages)-->
<link rel="stylesheet" href="{{asset('backend/css.css?family=Inter:300,400,500,600,700')}}">        <!--end::Fonts-->
<!--begin::Global Stylesheets Bundle(mandatory for all pages)-->
<link href="{{asset('backend/assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('backend/assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css">
                <!--end::Global Stylesheets Bundle-->
