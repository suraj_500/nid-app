 <!--begin::Sidebar-->
 <div id="kt_app_sidebar" class="app-sidebar  flex-column " data-kt-drawer="true" data-kt-drawer-name="app-sidebar" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="225px" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_app_sidebar_mobile_toggle">


     <!--begin::Logo-->
     <div class="app-sidebar-logo px-6" id="kt_app_sidebar_logo">
         <!--begin::Logo image-->
         <a href="index.html">
             <img alt="Logo" src="{{asset(getConfiguration('logo'))}}" class="h-50px app-sidebar-logo-default">
         </a>
         <!--end::Logo image-->


         <div id="kt_app_sidebar_toggle" class="app-sidebar-toggle btn btn-icon btn-shadow btn-sm btn-color-muted btn-active-color-primary h-30px w-30px position-absolute top-50 start-100 translate-middle rotate " data-kt-toggle="true" data-kt-toggle-state="active" data-kt-toggle-target="body" data-kt-toggle-name="app-sidebar-minimize">

             <i class="ki-duotone ki-black-left-line fs-3 rotate-180"><span class="path1"></span><span class="path2"></span></i>
         </div>
         <!--end::Sidebar toggle-->
     </div>
     <!--end::Logo-->
     <!--begin::sidebar menu-->
     <div class="app-sidebar-menu overflow-hidden flex-column-fluid">
         <!--begin::Menu wrapper-->
         <div id="kt_app_sidebar_menu_wrapper" class="app-sidebar-wrapper">
             <!--begin::Scroll wrapper-->
             <div id="kt_app_sidebar_menu_scroll" class="scroll-y my-5 mx-3" data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_app_sidebar_logo, #kt_app_sidebar_footer" data-kt-scroll-wrappers="#kt_app_sidebar_menu" data-kt-scroll-offset="5px" data-kt-scroll-save-state="true">
                 <!--begin::Menu-->
                 <div class="menu menu-column menu-rounded menu-sub-indention fw-semibold fs-6" id="#kt_app_sidebar_menu" data-kt-menu="true" data-kt-menu-expand="false">
                     <!--begin:Menu item-->
                     <div data-kt-menu-trigger="click" class="menu-item here show menu-accordion">
                         <!--begin:Menu link--><span class="menu-link"><span class="menu-icon"><i class="ki-duotone ki-element-11 fs-2"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i></span><span class="menu-title"><a class="menu-link active" href="{{route('dashboard.index')}}">Dashboards</a></span></span><!--end:Menu link--><!--begin:Menu sub-->
                     </div><!--end:Menu item--><!--begin:Menu item-->

                     <div data-kt-menu-trigger="click" class="menu-item here show menu-accordion">
                         <span class="menu-link"><span class="menu-icon"><i class="ki-duotone ki-element-11 fs-2"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i></span><span class="menu-title">Home Page</span><span class="menu-arrow"></span></span><!--end:Menu link--><!--begin:Menu sub-->
                         <div class="menu-sub menu-sub-accordion">
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-pop-notice.index')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">Pop Up Notice</span></a><!--end:Menu link-->
                             </div>
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-banner-notice.index')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">Banner Notice</span></a><!--end:Menu link-->
                             </div>
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-banner.index')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">Banner</span></a><!--end:Menu link-->
                             </div>
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-notice-category.index')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">Notice Category</span></a><!--end:Menu link-->
                             </div>
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-notice.index')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">Notice</span></a><!--end:Menu link-->
                             </div>
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-news.index')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">News</span></a><!--end:Menu link-->
                             </div>
                         </div>
                     </div>

                     <div data-kt-menu-trigger="click" class="menu-item here show menu-accordion">
                         <span class="menu-link"><span class="menu-icon"><i class="ki-duotone ki-element-11 fs-2"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i></span><span class="menu-title">Tender</span><span class="menu-arrow"></span></span><!--end:Menu link--><!--begin:Menu sub-->
                         <div class="menu-sub menu-sub-accordion">
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-tender-category.index')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">Tender Category</span></a><!--end:Menu link-->
                             </div>
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-tender.index')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">Tender</span></a><!--end:Menu link-->
                             </div>
                         </div>
                     </div>

                     <div data-kt-menu-trigger="click" class="menu-item here show menu-accordion">
                         <span class="menu-link"><span class="menu-icon"><i class="ki-duotone ki-element-11 fs-2"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i></span><span class="menu-title">Career</span><span class="menu-arrow"></span></span><!--end:Menu link--><!--begin:Menu sub-->
                         <div class="menu-sub menu-sub-accordion">
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-career-category.index')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">Career Category</span></a><!--end:Menu link-->
                             </div>
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-career-position.index')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">Career Position</span></a><!--end:Menu link-->
                             </div>
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-career.index')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">Career</span></a><!--end:Menu link-->
                             </div>
                         </div>
                     </div>

                     <div data-kt-menu-trigger="click" class="menu-item here show menu-accordion">
                         <span class="menu-link"><span class="menu-icon"><i class="ki-duotone ki-element-11 fs-2"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i></span><span class="menu-title">Members</span><span class="menu-arrow"></span></span><!--end:Menu link--><!--begin:Menu sub-->
                         <div class="menu-sub menu-sub-accordion">
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-designation.index')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">Designation</span></a><!--end:Menu link-->
                             </div>
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-department.index')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">Department</span></a><!--end:Menu link-->
                             </div>
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-staff.index')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">Staff</span></a><!--end:Menu link-->
                             </div>
                         </div>
                     </div>

                     <div data-kt-menu-trigger="click" class="menu-item here show menu-accordion">
                         <span class="menu-link"><span class="menu-icon"><i class="ki-duotone ki-element-11 fs-2"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i></span><span class="menu-title">Configuration</span><span class="menu-arrow"></span></span><!--end:Menu link--><!--begin:Menu sub-->
                         <div class="menu-sub menu-sub-accordion">
                             <div class="menu-item"><a class="menu-link active" href="{{route('settings')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">Settings</span></a><!--end:Menu link-->
                             </div>
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-about-us')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">About Us</span></a><!--end:Menu link-->
                             </div>
                         </div>
                     </div>

                     <div data-kt-menu-trigger="click" class="menu-item here show menu-accordion">
                         <span class="menu-link"><span class="menu-icon"><i class="ki-duotone ki-element-11 fs-2"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i></span><span class="menu-title">Contact</span><span class="menu-arrow"></span></span><!--end:Menu link--><!--begin:Menu sub-->
                         <div class="menu-sub menu-sub-accordion">
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-contact-detail.index')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">Site Contact Details</span></a><!--end:Menu link-->
                             </div>
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-contacted.index')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">Contacted Details</span></a><!--end:Menu link-->
                             </div>
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-report-information.index')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">Report Information</span></a><!--end:Menu link-->
                             </div>
                         </div>
                     </div>

                     <div data-kt-menu-trigger="click" class="menu-item here show menu-accordion">
                         <span class="menu-link"><span class="menu-icon"><i class="ki-duotone ki-element-11 fs-2"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i></span><span class="menu-title">Gallery</span><span class="menu-arrow"></span></span><!--end:Menu link--><!--begin:Menu sub-->
                         <div class="menu-sub menu-sub-accordion">
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-gallery.create')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">Create</span></a><!--end:Menu link-->
                             </div>
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-gallery.index')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">View</span></a><!--end:Menu link-->
                             </div>
                         </div>
                     </div>

                     <div data-kt-menu-trigger="click" class="menu-item here show menu-accordion">
                         <span class="menu-link"><span class="menu-icon"><i class="ki-duotone ki-element-11 fs-2"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i></span><span class="menu-title">Act / Regulations</span><span class="menu-arrow"></span></span><!--end:Menu link--><!--begin:Menu sub-->
                         <div class="menu-sub menu-sub-accordion">
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-regulation-category.index')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">Category</span></a><!--end:Menu link-->
                             </div>
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-regulation.index')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">Act/Regulation</span></a><!--end:Menu link-->
                             </div>
                         </div>
                     </div>

                     <div data-kt-menu-trigger="click" class="menu-item here show menu-accordion">
                         <span class="menu-link"><span class="menu-icon"><i class="ki-duotone ki-element-11 fs-2"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i></span><span class="menu-title">Training</span><span class="menu-arrow"></span></span><!--end:Menu link--><!--begin:Menu sub-->
                         <div class="menu-sub menu-sub-accordion">
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-training-role.index')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">Role</span></a><!--end:Menu link-->
                             </div>
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-training-department.index')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">Department</span></a><!--end:Menu link-->
                             </div>
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-trainer.index')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">Trainer</span></a><!--end:Menu link-->
                             </div>
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-gallery.index')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">Trainee</span></a><!--end:Menu link-->
                             </div>
                         </div>
                     </div>

                     <div data-kt-menu-trigger="click" class="menu-item here show menu-accordion">
                         <span class="menu-link"><span class="menu-icon"><i class="ki-duotone ki-element-11 fs-2"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i></span><span class="menu-title">Important Links</span><span class="menu-arrow"></span></span><!--end:Menu link--><!--begin:Menu sub-->
                         <div class="menu-sub menu-sub-accordion">
                             <div class="menu-item"><a class="menu-link active" href="{{route('admin-important-link.index')}}"><span class="menu-bullet"><span class="bullet bullet-dot"></span></span><span class="menu-title">Links</span></a><!--end:Menu link-->
                             </div>
                         </div>
                     </div>

                 </div>
                 <!--end::Menu wrapper-->
             </div>
             <!--end::sidebar menu-->

         </div>
         <!--end::Sidebar-->
