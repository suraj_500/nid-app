@extends('Frontend.layouts.app')
@section('content')
<section class="report-information py-md">
    <div class="container">
        <div class="row g-5">
            <div class="col-lg-6 col-12">
                <div class="card p-lg-5 p-4">
                    <h2 class="fs-32 text-black-800 fw-700 mb-4">Online Form</h2>
                    <form class="form-main" action="{{route('report-information.post')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                            <input type="text" name="full_name" class="form-control" id="" aria-describedby="" placeholder="Enter Your Name *">
                        </div>
                        <div class="mb-3">
                            <input type="number" name="phone" class="form-control" id="" placeholder="Phone Number* ">
                        </div>
                        <div class="mb-3">
                            <input type="email" name="email" class="form-control" id="" placeholder="Email Address* ">
                        </div>
                        <div class="mb-3 position-relative d-flex align-items-center">
                            <input type="file" name="file" class="form-control" id="" placeholder="Attach File">
                            <img src="{{ asset('Frontend/asset/icons/attach.svg') }}" alt="Attach Icon" height="26" width="26" class="attach-icon">
                        </div>
                        <div class="mb-4">
                            <textarea class="form-control" name="subject" id="" rows="5" placeholder="Your Message*"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
            <div class="col-lg-6 col-12">
                <div class="heading">
                    <div class="sub-heading d-flex gap-2 align-items-center mb-10">
                        <img src="{{ asset('Frontend/asset/icons/document.svg') }}" alt="Document" width="18" height="18">
                        <h6 class="text-primary-700 fw-600 mb-0">REPORT INFORMATION</h6>
                    </div>
                    <h2 class="fw-700 text-black-800">Instructions</h2>
                </div>
                <div class="accordion" id="accordionExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingOne">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <h4 class="fw-700 text-black-800 mb-0">
                                    What to Provide
                                </h4>
                            </button>
                        </h2>
                        <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <h5 class="text-black-700 fw-300">If you feel it is safe, think about including these details in your message:

                                    Your full name
                                    How you got the information you want to share with us
                                    How to contact you, including your home address and phone number
                                    CIA cannot guarantee a response to every message. We reply first to messages of most
                                    interest to us and to those with more detail. If we do respond to your message, we will do so
                                    using a secure method.</h5>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingTwo">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <h4 class="fw-700 text-black-800 mb-0">
                                    How to Report
                                </h4>
                            </button>
                        </h2>
                        <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <h5 class="text-black-700 fw-300">The best method to contact us depends on your personal situation. We will work to protect all information you give to us, including your identity. Our interactions with you will be respectful and professional. Depending on what you provide, we may offer you compensation.</h5>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingThree">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <h4 class="fw-700 text-black-800 mb-0">
                                    Report at anytime?
                                </h4>
                            </button>
                        </h2>
                        <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <h5 class="text-black-700 fw-300">If you feel it is safe, think about including these details in your message:

                                    Your full name
                                    How you got the information you want to share with us
                                    How to contact you, including your home address and phone number
                                    CIA cannot guarantee a response to every message. We reply first to messages of most
                                    interest to us and to those with more detail. If we do respond to your message, we will do so
                                    using a secure method. </h5>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingThree">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <h4 class="fw-700 text-black-800 mb-0">
                                    Feel safe to report Information?
                                </h4>
                            </button>
                        </h2>
                        <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <h5 class="text-black-700 fw-300">If you feel it is safe, think about including these details in your message:

                                    Your full name
                                    How you got the information you want to share with us
                                    How to contact you, including your home address and phone number
                                    CIA cannot guarantee a response to every message. We reply first to messages of most
                                    interest to us and to those with more detail. If we do respond to your message, we will do so
                                    using a secure method.</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
