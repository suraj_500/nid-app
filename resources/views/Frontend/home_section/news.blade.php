<section class="news">
    <div class="container">
        <div class="heading text-center">
            <div class="sub-heading text-center d-flex gap-2 align-items-center justify-content-center mb-10">
                <img src="{{ asset('Frontend/asset/icons/document.svg') }}" alt="Document" width="18" height="18">
                <h6 class="text-primary-700 fw-600 mb-0">NEWS</h6>
            </div>
            <h2 class="fw-700 text-black-800">Our News</h2>
        </div>
        <div class="news__slider">

            @foreach ($news as $news_data)
                <div class="news__card" data-aos="fade-up" data-aos-delay="">
                    <div class="p-4">
                        <img src="{{ asset('Admin/files/News') }}/{{ $news_data->image }}" alt="News" height="220"
                            width="100%" class="main-img object-fit-cover mb-4">
                        <h5 class="text-black-500 mb-12">{{ date('d M Y', strtotime($news_data->published_date)) }}</h5>
                        <h3 class="text-black-700 fw-600">{{ $news_data->title }}</h3>
                        <p class="text-black-500 mb-12">{!! Str::words($news_data->description, '35') !!}</p>
                        <div class="">
                            <a href="{{ route('news-detail', $news_data->slug) }}"
                                class="d-flex align-items-center gap-3">
                                <span class="hfs5 text-primary-700 fw-600">Read More</span>
                                <img src="{{ asset('Frontend/asset/icons/arrow-right.svg') }}" alt="Arrow Right"
                                    height="10" width="20">
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</section>
