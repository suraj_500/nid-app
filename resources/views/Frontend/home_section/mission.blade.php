{{-- <div class="more-info-modal modal fade" id="moreInfoModal" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg modal-more-info ">
        <div class="modal-content p-lg-4 p-3">
            <div class="text-end">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row g-xl-4 g-3 align-items-center">
                    <div class="col-lg-4 col-12">
                        <div class="card p-3 d-flex flex-column align-items-center justify-content-center">
                            <img src="{{ asset('Frontend/asset/images/mission.png') }}" alt="Person" width="150"
height="150" class="mb-3">
<h5 class="fw-600 text-primary-700 mb-0">Ramesh Aryal</h5>
</div>
</div>
<div class="col-lg-8 col-12">
    <div class="d-flex gap-md-3 gap-2 flex-column">
        <div class="row g-2 align-items-center">
            <div class="col-md-1 col-1">
                <img src="{{ asset('Frontend/asset/icons/user.svg') }}" alt="Person" width="auto" height="auto">
            </div>
            <div class="col-4">
                <h5 class="fw-400 text-black-500 mb-0">Designation </h5>
            </div>
            <div class="col-1">
                <h5 class="p text-grey-600 mb-0">:</h5>
            </div>
            <div class="col-6">
                <h5 class="p fw-500 text-grey-600 mb-0">Officer </h5>
            </div>
        </div>
        <div class="row g-2 align-items-center">
            <div class="col-md-1 col-1">
                <img src="{{ asset('Frontend/asset/icons/organization.svg') }}" alt="Organization" width="auto" height="auto">
            </div>
            <div class="col-4">
                <h5 class="fw-400 text-black-500 mb-0">Organization </h5>
            </div>
            <div class="col-1">
                <h5 class="p text-grey-600 mb-0">:</h5>
            </div>
            <div class="col-6">
                <h5 class="p fw-500 text-grey-600 mb-0">NIA</h5>
            </div>
        </div>
        <div class="row g-2 align-items-center">
            <div class="col-md-1 col-1">
                <img src="{{ asset('Frontend/asset/icons/email.svg') }}" alt="Email" width="auto" height="auto">
            </div>
            <div class="col-4">
                <h5 class="fw-400 text-black-500 mb-0">Email </h5>
            </div>
            <div class="col-1">
                <h5 class="p text-grey-600 mb-0">:</h5>
            </div>
            <div class="col-6">
                <h5 class="p fw-500 text-grey-600 mb-0">nia@gmail.com</h5>
            </div>
        </div>
        <div class="row g-2 align-items-center">
            <div class="col-md-1 col-1">
                <img src="{{ asset('Frontend/asset/icons/phone.svg') }}" alt="Phone" width="auto" height="auto">
            </div>
            <div class="col-4">
                <h5 class="fw-400 text-black-500 mb-0">Phone Number </h5>
            </div>
            <div class="col-1">
                <h5 class="p text-grey-600 mb-0">:</h5>
            </div>
            <div class="col-6">
                <h5 class="p fw-500 text-grey-600 mb-0">981178456</h5>
            </div>
        </div>
        <!-- More fields -->
    </div>
</div>
</div>
</div>
</div>
</div>
</div> --}}
<section class="mission">
    <div class="container">
        <div class="mission__row d-flex align-items-center flex-column flex-xl-row position-relative">
            <div class="w-50">
                <div class="heading mb-0">
                    <div class="sub-heading d-flex gap-2 mb-14">
                        <img src="{{ asset('Frontend/asset/icons/mission.svg') }}" alt="Mission" width="18" height="18">
                        <h6 class="text-primary-700 fw-600 mb-0">MISSION</h6>
                    </div>
                    <h2 class="fw-700 text-black-800 mb-14">{{getAboutus('mission_title')}}</h2>
                </div>
                <p class="fw-400 text-black-500 mb-14">{{getAboutus('mission_description')}}</p>
            </div>
            <div class="w-50">
                <div class="row g-xl-4 g-3">

                    @foreach ($staff as $staff_detail)
                    <div class="col-lg-6 col-12" data-aos="fade-up">
                        <div class="card d-flex flex-column justify-content-center align-items-center">
                            <img src="{{asset('Admin/files/Staff')}}/{{$staff_detail->image}}" alt="Chairman" height="90" width="90" class="object-fit-cover mb-1">
                            <h4 class="fw-700 text-black-800 mb-0">{{$staff_detail->name}}</h4>
                            <h5 class="hfs7 fw-400 text-black-500 mb-0">{{optional($staff_detail->designation)->name}}</h5>
                            <!-- <a type="link" class="h6 text-primary-700 fw-400 text-decoration-underline mb-0 cursor-pointer" data-bs-toggle="modal" data-bs-target="#moreInfoModal">
                                More Info
                            </a> -->
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</section>
