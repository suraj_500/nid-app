<section class="banner__slider" style="overflow: hidden;">
    @foreach ($banner as $banner_data)
    <div class="slide-one d-flex justify-content-center align-items-end" style="padding: 38px 0;
    background: linear-gradient(
        180deg,
        rgba(15, 69, 150, 0) -2.27%,
        rgba(15, 69, 150, 0.6) 95.96%
      ),
      url('{{ asset("Admin/files/Banner/" . $banner_data->image) }}') lightgray 50%;
    height: 740px;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;">
        <div class="container">
            <div class="col-lg-10 col-12 m-auto">
                <h2 class="text-white fw-700 text-center">{{$banner_data->title}}</h2>
                <p class="fw-500 text-white text-center mb-5">{!!$banner_data->description!!}</p>
            </div>
        </div>
    </div>
    @endforeach
</section>
