<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal1">
    Notice Popup
</button>

<div class="modal fade  more-link-modal" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel1"
    aria-modal="true" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title text-black-800 fw-600" id="exampleModalLabel1">
                    Notice
                </h3>
            </div>
            <div class="modal-body">
                <div class="notice-slider">
                    <img src="{{ asset('Frontend/asset/images/about-2.png') }}" alt="Notice" width=""
                        height="" class="img-fluid">
                    <img src="{{ asset('Frontend/asset/images/about-2.png') }}" alt="Notice" width=""
                        height="" class="img-fluid">
                </div>
            </div>
            <button type="button" class="btn-close modal-btn" data-bs-dismiss="modal" aria-label="Close">
                <div>
                    <img src="{{ asset('Frontend/asset/icons/close.svg') }}" alt="Close Icon" height="18"
                        width="18">
                </div>
                <div class="modal-btn__close">close</div>
            </button>
        </div>
    </div>
</div>
