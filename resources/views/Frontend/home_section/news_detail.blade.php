@extends('Frontend.layouts.app')
@section('content')
    <section class="news-event py-md">
        <div class="container">
            <div class="heading text-center">
                <div class="sub-heading text-center d-flex gap-2 align-items-center justify-content-center mb-10">
                    <img src="{{ asset('Frontend/asset/icons/document.svg') }}" alt="Document" width="18"
                        height="18">
                    <h6 class="text-primary-700 fw-600 mb-0">NEWS</h6>
                </div>
                <h2 class="fw-700 text-black-800">News Detail</h2>
            </div>
            <div class="news-detail">
                <div class="mb-4">
                    <img src="{{asset('Admin/files/News')}}/{{$news_detail->image}}" alt="News Detail" width="100%"
                        height="600" class="object-fit-cover main-img">
                </div>
                <div>
                    <div class="d-flex align-items-center gap-2 mb-3">
                        <img src="{{ asset('Frontend/asset/icons/calendar.svg') }}" alt="Calendar" width="18"
                            height="18">
                        <h6 class="text-primary-800 mb-0">{{ date('d M Y', strtotime($news_detail->published_date)) }}</h6>
                    </div>
                    <h3 class="fw-700 text-black-800 mb-3">{{$news_detail->title}}</h3>
                    <p class="fw-400 text-black-500 mb-14">
                        {!!$news_detail->description!!}
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection
