@extends('Frontend.layouts.app')
@section('content')
    @include('Frontend.home_section.banner')
    @include('Frontend.home_section.mission')
    @include('Frontend.home_section.notices')
    @include('Frontend.home_section.news')
    @include('Frontend.home_section.notice_popup')
@endsection
