@extends('Frontend.layouts.app')
@section('content')
<section class="career py-md">
    <div class="container">
        <div class="heading text-center">
            <div class="sub-heading text-center d-flex gap-2 align-items-center justify-content-center mb-10">
                <img src="{{asset('Frontend/asset/icons/document.svg')}}" alt="Document" width="18" height="18">
                <h6 class="text-primary-700 fw-600 mb-0">Act/Regulation</h6>
            </div>
            <h2 class="fw-700 text-black-800">Our Act/Regulation Details</h2>
        </div>
        <div class="tabs-table-section">
            <ul class="nav nav-pills justify-content-center mb-4 gap-4" id="pills-tab" role="tablist">
                @foreach($regulation_categories as $category)
                <li class="nav-item" role="presentation">
                    <button class="nav-link @if($loop->first) active @endif" id="pills-{{ $category->id }}-tab" data-bs-toggle="pill" data-bs-target="#pills-{{ $category->id }}" type="button" role="tab" aria-controls="pills-{{ $category->id }}" aria-selected="{{ $loop->first ? 'true' : 'false' }}">{{ $category->title }}</button>
                </li>
                @endforeach
            </ul>

            <div class="tab-content" id="pills-tabContent">
                @foreach($regulation_categories as $category)
                <div class="tab-pane fade @if($loop->first) show active @endif" id="pills-{{ $category->id }}" role="tabpanel" aria-labelledby="pills-{{ $category->id }}-tab">
                    <div class="table-responsive">
                        <table class="table table-main">
                            <tbody>
                                @foreach($category->regulations as $regulation)
                                <tr>
                                    <td>

                                        <div class="d-flex align-items-start  gap-xl-4 gap-lg-4 gap-md-3 gap-2">
                                            <img src="{{asset('Frontend/asset/icons/notice.svg')}}" alt="Notice" height="25" width="25">
                                            <div>
                                                <h6 class="fw-600 text-black-800 mb-0">
                                                    {{ $regulation->title }}
                                                </h6>
                                                <h6 class="fw-400 text-black-800 mb-0">{!! $regulation->description !!}</h6>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="details d-flex justify-content-end">
                                        <div class="d-flex  gap-xl-4 gap-lg-4 gap-md-3 gap-2 align-items-center">
                                            <h6 class="text-black-400 mb-0 time">{{ $regulation->created_at->diffForHumans() }}</h6>
                                            <a href="{{ asset('Admin/files/Regulation/' . $regulation->image) }}" target="__blank" title="View Regulation">
                                            <img src="{{ asset('Frontend/asset/icons/eye.svg') }}" alt="Download icon" height="18" width="18">
                                            </a>
                                            <a href="{{ asset('Admin/files/Career/' . $regulation->image) }}" download title="Download Regulation">
                                                <div class="d-flex gap-lg-2 gap-1 align-items-center">
                                                    <img src="{{asset('Frontend/asset/icons/download.svg')}}" alt="Download icon" height="18" width="18">
                                                    <!-- <h4 class="mb-0 text-primary-700 fw-600 text-decoration-underline">
                                                        Download</h4> -->
                                                </div>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
@endsection
