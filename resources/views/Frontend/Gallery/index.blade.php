@extends('Frontend.layouts.app')

@section('content')
<section class="gallery py-md">
    <div class="container">
        <div class="heading text-center">
            <div class="sub-heading text-center d-flex gap-2 align-items-center justify-content-center mb-10">
                <img src="{{ asset('Frontend/asset/icons/document.svg') }}" alt="Document" width="18" height="18">
                <h6 class="text-primary-700 fw-600 mb-0">GALLERY</h6>
            </div>
            <h2 class="fw-700 text-black-800">Our Gallery</h2>
        </div>
        <div class="tabs-table-section">
            <ul class="nav nav-pills justify-content-center mb-4 gap-4" id="pills-tab" role="tablist">
                @foreach ($galleries as $index => $gallery)
                    <li class="nav-item" role="presentation">
                        <button class="nav-link @if($index == 0) active @endif" id="pills-tab-{{ $gallery->id }}" data-bs-toggle="pill"
                                data-bs-target="#pills-content-{{ $gallery->id }}" type="button" role="tab"
                                aria-controls="pills-content-{{ $gallery->id }}" aria-selected="{{ $index == 0 ? 'true' : 'false' }}">
                            {{ $gallery->title }}
                        </button>
                    </li>
                @endforeach
            </ul>

            <div class="tab-content" id="pills-tabContent">
                @foreach ($galleries as $index => $gallery)
                    <div class="tab-pane fade @if($index == 0) show active @endif" id="pills-content-{{ $gallery->id }}" role="tabpanel"
                         aria-labelledby="pills-tab-{{ $gallery->id }}" tabindex="0">
                        <div class="main">
                            <div class="row g-3">
                                @foreach ($gallery->galleryImages as $image)
                                    <div class="col-lg-4 col-12">
                                        <img src="{{ asset($image->image) }}" alt="Gallery Image" width="100%" height="300" class="object-fit-cover">
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
@endsection
