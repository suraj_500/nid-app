<link rel="stylesheet" href="{{asset('Frontend/css/main.css')}}" />
<link rel="stylesheet" href="{{asset('Frontend/css/package/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('Frontend/css/package/slick.css')}}">
<link rel="stylesheet" href="{{asset('Frontend/css/package/slick-theme.css')}}">
<link rel="stylesheet" href="{{asset('Frontend/css/package/font.css')}}">
<link rel="stylesheet" href="{{asset('Frontend/css/package/aos.css')}}">
<link rel="stylesheet" href="{{asset('Frontend/css/variables.css')}}">
<link rel="stylesheet" href="{{asset('Frontend/css/global.css')}}">
<link rel="stylesheet" href="{{asset('Frontend/css/style.css')}}">

