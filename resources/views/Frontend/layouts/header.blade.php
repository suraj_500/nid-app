<header>
    <div class="top__header">
        <div class="container">
            <div class="d-flex justify-content-between flex-column flex-md-row">
                <div class="d-flex align-items-center gap-3">
                    <img src="{{ asset(getConfiguration('logo')) }}" alt="Logo" height="auto" width="90" class="logo">
                    <div>
                        <h6 class="text-primary-800 fw-300 mb-0">
                            नेपाल सरकार
                            <br>
                            प्रधानमन्त्री तथा मन्त्रिपरिषद्को कार्यालय
                            <br>
                            <span class="text-primary-800 fw-600">
                                राष्ट्रिय अनुसन्धान विभाग
                            </span>
                        </h6>
                    </div>
                </div>
                <div class="d-flex align-items-start align-items-md-center flex-column flex-md-row top__header__right">
                    <div class="d-flex align-items-center gap-1">
                        <img src="{{ asset('Frontend/asset/icons/calendar.svg') }}" alt="Calendar" height="20" width="20">
                        <h6 class="fw-500 text-black-700 mb-0">11 Push 2080, Wednesday</span>
                    </div>
                    <div class="d-flex align-items-center gap-3">
                        <span class="text-black-400">English</span>
                        <div class="form-check form-switch">
                            <input class="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckDefault">
                        </div>
                        <span class="text-primary-800">Nepali</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="navbar navbar-expand-lg bg-primary-800 py-0">
        <div class="container">
            <button class="navbar-toggler navbar-dark" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse mb-4 mb-lg-0" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('/') ? 'active' : '' }}" aria-current="page" href="{{ route('/') }}">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('about-us') ? 'active' : '' }}" aria-current="page" href="{{ route('about-us') }}">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('regulation') ? 'active' : '' }}" aria-current="page" href="{{ route('regulation') }}">Act/Regulation</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('career') ? 'active' : '' }}" aria-current="page" href="{{ route('career') }}">Career</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('notice') ? 'active' : '' }}" aria-current="page" href="{{ route('notice') }}">Notice</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('contact-us') ? 'active' : '' }}" aria-current="page" href="{{ route('contact-us') }}">Contact Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('gallery') ? 'active' : '' }}" aria-current="page" href="{{ route('gallery') }}">Gallery</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('report-information') ? 'active' : '' }}" aria-current="page" href="{{ route('report-information') }}">Report Information </a>
                    </li>
                </ul>
                <div class="d-flex gap-4">
                    <img src="{{ asset('Frontend/asset/images/flag-nepal.gif') }}" alt="Nepal flag" width="28" height="34">
                    <a href="{{ route('training.page') }}" class="btn btn-secondary">Login</a>
                </div>

            </div>
        </div>
    </nav>
</header>
