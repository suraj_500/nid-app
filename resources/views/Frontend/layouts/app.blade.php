<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="icon" href="{{ asset(getConfiguration('fab_icon')) }}">
    <meta name="description" content="NID Website">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>NID</title>
    @include('Frontend.layouts.styles')
</head>

<body>
    @include('Frontend.layouts.header')
    @include('Frontend.layouts.notice')
    @yield('content')
    @include('Frontend.layouts.footer')
</body>

@include('Frontend.layouts.scripts')

</html>
