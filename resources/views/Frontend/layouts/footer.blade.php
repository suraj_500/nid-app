@php
    use Carbon\Carbon;

    $currentDate = Carbon::now();
    $current_date = $currentDate->format('Y - m - d');
    $current_time = $currentDate->format('H : i : s');
@endphp
<div class="more-link-modal">
    <div class="modal fade show" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel2" aria-modal="true"
        role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title text-black-800 fw-600" id="exampleModalLabel">
                        More Important Links
                    </h3>
                </div>
                <div class="modal-body">
                    <ul>

                    @foreach (getImportantlinks() as $links)
                        <li>
                        <a href="{{ $links->url }}" target="__blank">
                            <h5 class="text-black-500 fw-400">{{ $links->title }}</h5>
                        </a>
                        </li>
                    @endforeach

                    </ul>
                </div>
                <button type="button" class="btn-close modal-btn" data-bs-dismiss="modal" aria-label="Close">
                    <div>
                        <img src="{{ asset('Frontend/asset/icons/close.svg') }}" alt="Close Icon" height="18"
                            width="18">
                    </div>
                    <div class="modal-btn__close">close</div>
                </button>
            </div>
        </div>
    </div>
</div>
<footer class="bg-primary-800 position-relative">
    <img src="{{ asset('Frontend/asset/images/footer-bottom.png') }}" alt="Pattern" class="pattern" height="auto"
        width="100%">
    <div class="container">
        <div class="row g-lg-5 g-4 mb-5">
            <div class="col-xl-5 col-12">
                <div
                    class="card bg-primary-700 d-flex flex-column flex-lg-row p-lg-4 p-3 gap-4 align-items-start align-items-lg-center">
                    <img src="{{ asset(getConfiguration('footer_logo')) }}" alt="Logo" height="104"
                        width="124">
                    <div>
                        <h5 class="hfs7 fw-500 text-primary-100 mb-10">{{ getConfiguration('contact_address') }}</h5>
                        <div class="">
                            <h6 class="text-primary-100 fw-400">Email <span
                                    class="text-primary-50 fw-700">:{{ getConfiguration('contact_email_primary') }}</span>
                            </h6>
                        </div>
                        <div class="">
                            <h6 class="text-primary-100 fw-400">Call Us At <span class="text-primary-50 fw-700">:
                                    {{ getConfiguration('contact_phone_primary') }}</span></h6>
                        </div>
                        <div class="mb-3">
                            <h6 class="text-primary-100 fw-400">Fax <span class="text-primary-50 fw-700">:
                                    {{ getConfiguration('fax') }}</span></h6>
                        </div>
                        <div>
                            <h5 class="hfs7 fw-500 text-primary-100 mb-10">Contact info related to online application
                                system</h5>
                            <div class="">
                                <h6 class="text-primary-100 fw-400">Email <span class="text-primary-50 fw-700">:
                                        {{ getConfiguration('contact_email_secondary') }}</span></h6>
                            </div>
                            <div class="">
                                <h6 class="text-primary-100 fw-400">Call Us At <span class="text-primary-50 fw-700">:
                                        {{ getConfiguration('contact_phone_secondary') }}</span></h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-12 links">
                <div class="d-flex justify-content-between links flex-column flex-xl-row gap-4">
                    <ul>
                        <li>
                            <h4 class="text-black-50 fw-700 mb-4">Support</h4>
                        </li>
                        <li>
                            <a href="{{ route('/') }}">
                                <h5 class="fw-400 mb-4">Home</h5>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('about-us') }}">
                                <h5 class="fw-400 mb-4">About Us</h5>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('tender') }}">
                                <h5 class="fw-400 mb-4">Tender</h5>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('career') }}">
                                <h5 class="fw-400 mb-4">Career</h5>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('notice') }}">
                                <h5 class="fw-400 mb-4">Notice</h5>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('contact-us') }}">
                                <h5 class="fw-400 mb-4">Contact Us</h5>
                            </a>
                        </li>
                        <!-- <li>
                            <a href="" type="button" data-bs-toggle="modal" data-bs-target="#exampleModal1">
                                <h5 class="fw-400 mb-4">More Links</h5>
                            </a>
                        </li> -->
                    </ul>
                    <ul>
                        <li>
                        <a href="" type="button" data-bs-toggle="modal" data-bs-target="#exampleModal1">
                            <h4 class="text-black-50
                                fw-700 mb-4">Important Links</h4>
                        </a>
                        </li>
                        @foreach (getImportantlinkslimit() as $links)
                            <li>
                                <a href="{{ $links->url }}" target="__blank">
                                    <h5 class="fw-400 mb-4">{{ $links->title }}</h5>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-xl-3 col-12">
                <iframe src="{{ getConfiguration('map') }}" title="Map" width="100%" height="100%"
                    style="border:0;" allowfullscreen="" loading="lazy"
                    referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
        </div>
        <hr class="">
        <div class="d-flex justify-content-between flex-column flex-lg-row gap-3 footer__bottom mt-4">
            <div class="d-flex gap-1 gap-sm-3 align-items-start align-items-sm-center flex-column flex-sm-row">
                <h4 class="text-primary-100 mb-0">Latest Update :
                    <h4 class="text-primary-50 date fw-600 fs-22 mb-0">{{ $current_date }}</h4>
                    <h4 class="text-primary-700 bg-primary-100 time fw-600 fs-22 mb-0">{{ $current_time }}</h4>
                </h4>
            </div>
            <div class="d-flex align-items-center footer__bottom-right">
                <h4 class="text-black-50 mb-0">{{ getConfiguration('copyright') }}</h4>
                <button id="myBtn" title="Go to top">
                    <img src="{{ asset('Frontend/asset/icons/totop.svg') }}" alt="Button to top" height="50"
                        width="50">
                </button>
            </div>
        </div>
    </div>
</footer>
