<div class="bg-gray-25">
    <div class="container">
        <section class="motion-text">
            <div class="d-flex align-items-center">
                <h5 class="title bg-red text-white text-center mb-0">
                    Notice
                </h5>
                <marquee behavior="scroll" direction="left">
                        @foreach (getNotice() as $notice_banner)
                        <span class="mb-0 mt-0 py-0 fw-400 hfs6 me-5">
                            {{ $notice_banner->title }}
                        </span>
                        @endforeach
                    </marquee>
            </div>
        </section>
    </div>
</div>
