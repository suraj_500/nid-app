@extends('Frontend.layouts.app')
@section('content')
<section class="tender py-md">
    <div class="container">
        <div class="heading text-center">
            <div class="sub-heading text-center d-flex gap-2 align-items-center justify-content-center mb-10">
                <img src="{{asset('Frontend/asset/icons/document.svg')}}" alt="Document" width="18" height="18">
                <h6 class="text-primary-700 fw-600 mb-0">TENDER</h6>
            </div>
            <h2 class="fw-700 text-black-800">Our Tender Details</h2>
        </div>
        <div class="tabs-table-section">
            <div class="table-responsive">
                <table class="table table-main">
                    <tbody>
                        @foreach ($tender as $tender_detail)

                        <tr>
                            <td>
                                <a href="{{ asset('Admin/files/Tender/' . $tender_detail->image) }}" target="__blank">
                                    <div class="d-flex align-items-start  gap-xl-4 gap-lg-4 gap-md-3 gap-2">
                                        <img src="{{asset('Frontend/asset/icons/notice.svg')}}" alt="Notice" height="25" width="25">
                                    <div>
                                        <h6 class="fw-600 text-black-800 mb-0">
                                            {{$tender_detail->title}}</h6>
                                        <h6 class="fw-400 text-black-800 mb-0">{!!$tender_detail->description!!}</h6>
                                    </div>
                                </div>
                            </a>
                            </td>
                            <td class="details d-flex justify-content-end">
                                <div class="d-flex  gap-xl-4 gap-lg-4 gap-md-3 gap-2 align-items-center">
                                    <h5 class="text-primary-700 fw-600 mb-0 py-1 px-3 bg-primary-50 text-center type">
                                        {{optional($tender_detail->tendercategory)->title}}</h5>
                                    <h6 class="text-black-400 mb-0 time"> 2 Hour Ago</h6>
                                    <a href="{{ asset('Admin/files/Tender/' . $tender_detail->image) }}" download>
                                        <div class="d-flex gap-lg-2 gap-1 align-items-center">
                                            <img src="{{asset('Frontend/asset/icons/download.svg')}}" alt="Download icon" height="18" width="18">
                                            <h4 class="mb-0 text-primary-700 fw-600 text-decoration-underline">
                                                Download</h4>
                                        </div>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
            <!-- pagination -->
            <nav aria-label="Page navigation example">
                {{ $tender->links() }}
                <!-- <ul class="pagination justify-content-end">
                    <li class="page-item">
                        <a class="page-link arrow" href="#" aria-label="Previous">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="">
                                <path d="M11.5247 15.8334C11.2726 15.8342 11.0336 15.7209 10.8747 15.525L6.84973 10.525C6.59697 10.2175 6.59697 9.77421 6.84973 9.46671L11.0164 4.46671C11.3109 4.11233 11.837 4.06383 12.1914 4.35838C12.5458 4.65293 12.5943 5.179 12.2997 5.53338L8.57473 10L12.1747 14.4667C12.3828 14.7165 12.4266 15.0645 12.287 15.358C12.1474 15.6516 11.8498 15.8372 11.5247 15.8334Z" fill="" />
                            </svg>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link arrow" href="#" aria-label="Next">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="">
                                <path d="M8.33334 15.8334C8.13863 15.8337 7.94993 15.7659 7.8 15.6417C7.62958 15.5004 7.52238 15.2971 7.50206 15.0767C7.48173 14.8562 7.54996 14.6368 7.69167 14.4667L11.425 10L7.825 5.52504C7.6852 5.35287 7.61978 5.13208 7.64324 4.91155C7.6667 4.69102 7.7771 4.48893 7.95 4.35004C8.12431 4.19667 8.35469 4.12297 8.58565 4.1467C8.81661 4.17043 9.02719 4.28943 9.16667 4.47504L13.1917 9.47504C13.4444 9.78254 13.4444 10.2259 13.1917 10.5334L9.025 15.5334C8.85545 15.7379 8.59853 15.8493 8.33334 15.8334Z" fill="" />
                            </svg>
                        </a>
                    </li>
                </ul> -->
            </nav>
        </div>
    </div>
</section>
@endsection
