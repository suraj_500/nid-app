@extends('Frontend.layouts.app')
@section('content')
    <section class="about">
        <div class="container">
            <div class="about-main py-lg d-flex align-items-center">
                <div class="w-50 position-relative" data-aos="fade-right" data-aos-delay="">
                    <div class="row g-3 g-md-4">
                        <div class="col-8">
                            {{-- <img src="{{ asset(getAboutus('image_two')) }}" alt="About Image" width="100%" height="434"
                                class="object-fit-cover main"> --}}
                            <div class="main"></div>
                            <img src="{{ asset('Frontend/asset/images/elipse.png') }}" alt="Elipse" class="elipse">
                        </div>
                        <div class="col-4">
                            <img src="{{ asset(getAboutus('image_one')) }}" alt="About Image" width="100%" height="218"
                                class="object-fit-cover logo">
                        </div>
                    </div>
                    <div class="main-two">
                        {{-- <img src="{{ asset(getAboutus('image_three')) }}" alt="About Image" width="370" height="296"
                            class="object-fit-cover"> --}}
                        <div class="image">
                        </div>
                    </div>
                </div>
                <div class="w-50" data-aos="fade-left" data-aos-delay="300">
                    <div class="heading mb-0">
                        <div class="sub-heading d-flex gap-2 mb-3">
                            <img src="{{ asset('Frontend/asset/icons/document.svg') }}" alt="Document" width="18"
                                height="18">
                            <h6 class="text-primary-700 fw-600 mb-0">ABOUT US</h6>
                        </div>
                        <h2 class="fw-700 text-black-800 mb-3">{{ getAboutus('first_title') }}</h2>
                    </div>
                    <p class="fw-400 text-black-500 word-wrap-break">{{ getAboutus('first_description') }}</p>
                </div>
            </div>
            <div class="about-main py-lg d-flex align-items-center">
                <div class="w-50 position-relative" data-aos="fade-right" data-aos-delay="">
                    <div class="row g-3 g-md-4">
                        <div class="col-8">
                            {{-- <img src="{{ asset(getAboutus('image_two')) }}" alt="About Image" width="100%" height="434"
                                class="object-fit-cover main"> --}}
                            <div class="main"></div>
                            <img src="{{ asset('Frontend/asset/images/elipse.png') }}" alt="Elipse" class="elipse">
                        </div>
                        <div class="col-4">
                            <img src="{{ asset(getAboutus('image_one')) }}" alt="About Image" width="100%" height="218"
                                class="object-fit-cover logo">
                        </div>
                    </div>
                    <div class="main-two">
                        {{-- <img src="{{ asset(getAboutus('image_three')) }}" alt="About Image" width="370" height="296"
                            class="object-fit-cover"> --}}
                        <div class="image">
                        </div>
                    </div>
                </div>
                <div class="w-50" data-aos="fade-left" data-aos-delay="300">
                    <div class="heading mb-0">
                        <div class="sub-heading d-flex gap-2 mb-3">
                            <img src="{{ asset('Frontend/asset/icons/document.svg') }}" alt="Document" width="18"
                                height="18">
                            <h6 class="text-primary-700 fw-600 mb-0">ABOUT US</h6>
                        </div>
                        <h2 class="fw-700 text-black-800 mb-3">{{ getAboutus('first_title') }}</h2>
                    </div>
                    <p class="fw-400 text-black-500 word-wrap-break">{{ getAboutus('first_description') }}</p>
                </div>
            </div>
        </div>
    </section>
@endsection
