@extends('Frontend.layouts.app')
@section('content')
<section class="contact py-md">
    <div class="container">
        <div class="row g-xl-5 g-4 align-items-center">
            <div class="col-xl-6 col-12" data-aos="fade-right" data-aos-delay="">
                <div class="heading mb-0">
                    <div class="sub-heading d-flex gap-2 mb-2">
                        <img src="{{asset('Frontend/asset/icons/document.svg')}}" alt="Document" width="18" height="18">
                        <h6 class="text-primary-700 fw-600 mb-0">CONTACT US</h6>
                    </div>
                    <h2 class="fw-700 text-black-800 mb-20">Get In Touch With Us</h2>
                </div>

                @foreach ($contact_detail as $contact_detail)
                <div class="contact__card">
                    <div class="d-flex align-items-start align-items-sm-center justify-content-between gap-2 flex-column flex-sm-row">
                        <div class="w-70">
                            <h4 class="text-black-700 mb-0 fw-400">{{$contact_detail->location}}</h4>
                        </div>
                        <div class="w-30">
                            <div class="d-flex gap-3 align-items-center">
                                <img src="{{asset('Frontend/asset/icons/call.svg')}}" alt="Call Icon" height="34" width="34">
                                <h4 class="text-primary-700 fw-700 mb-0">{{$contact_detail->phone}}</h4>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
            <div class="col-xl-6 col-12" data-aos="fade-left" data-aos-delay="300">
                <div class="contact__card-two">
                    <h2 class="text-black-800 fw-700 mb-4">Connect With Us</h2>
                    <h4 class="text-yellow fw-300 mb-4">* Note :<span class="fw-400"> The information will be kept
                            confidential.</span>
                    </h4>
                    <form action="{{route('contact-us.post')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                            <input type="text" name="full_name" class="form-control" id="" aria-describedby="" placeholder="Enter Your Full Name *">
                        </div>
                        <div class="mb-3">
                            <input type="text" name="phone" class="form-control" id="" placeholder="Phone Number/Email ">
                        </div>
                        <div class="mb-3 position-relative d-flex align-items-center">
                            <input type="text" name="file" class="form-control" id="" placeholder="Subject*">
                            <!-- <img src="{{asset('Frontend/asset/icons/attach.svg')}}" alt="Attach Icon" height="26" width="26" class="attach-icon"> -->
                        </div>
                        <div class="mb-4">
                            <textarea class="form-control" id="" name="subject" rows="5" placeholder="Description *"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
