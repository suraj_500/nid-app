@extends('Admin.layouts.app')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Dashboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('admin-dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="card">
        <div class="card-body row">
            <div class="col-5 text-center d-flex align-items-center justify-content-center">
                <div class="">
                    <h2>Admin<strong>PANEL</strong></h2>
                    <p class="lead mb-5">Profile DetailS
                    </p>
                </div>
            </div>

            <div class="col-7">
                <form action="{{route('adminupdateprofile',$adminDetails->id)}}" method="post"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="inputName">First Name</label>
                        <input type="text" id="inputName" name="first_name" value="{{$adminDetails->first_name}}"
                            class="form-control" placeholder="Enter First Name" />
                    </div>
                    <div class="form-group">
                        <label for="inputName">Last Name</label>
                        <input type="text" id="inputName" name="last_name" value="{{$adminDetails->last_name}}"
                            class="form-control" placeholder="Enter Last Name" />
                    </div>
                    <div class="form-group">
                        <label for="inputEmail">E-Mail</label>
                        <input type="text" id="inputEmail" name="email" value="{{$adminDetails->email}}"
                            class="form-control" placeholder="" disabled="true" />
                    </div>
                    <div class="form-group">
                        <label for="inputMessage">Profile Image</label>
                        <input type="file" id="inputSubject" name="image" class="form-control" onchange="readURL(this);"
                            accept="image/" />
                        <img src="{{asset('/Admin/Profile/'.$adminDetails->image)}}" width="100px" id="one">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" type="submit">Update</button>
                    </div>
                </form>
            </div>

        </div>
    </div>

</section>
<!-- /.content -->
@endsection

@push('js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
    function readURL(input){
    if(input.files && input.files[0]){
      var reader =new FileReader();
      reader.onload=function(e){
        $("#one").attr('src',e.target.result).width(100)
      };
      reader.readAsDataURL(input.files[0]);
    }
  }
</script>
@endpush
