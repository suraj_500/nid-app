@extends('Admin.layouts.app')
@push('css')
<link rel="stylesheet" href="{{asset('Backend/plugins/toastr/toastr.min.css')}}" />
@endpush
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Dashboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('admin-dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="card">
        <div class="card-body row">
            <div class="col-5 text-center d-flex align-items-center justify-content-center">
                <div class="">
                    <h2>Admin<strong>PANEL</strong></h2>
                    <p class="lead mb-5">Change Password
                    </p>
                </div>
            </div>
            <div class="col-7">
                <!-- {{-- @include('Admin.admin.profile._message') --}} -->
                <form action="{{route('adminupdatepassword',$user->id)}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="inputName">Current Password</label>
                        <input type="password" id="current_password" name="current_password" value=""
                            class="form-control" placeholder="Enter Old Password" />

                    </div>
                    <div class="form-group">
                        <label for="inputEmail">New-Password</label>
                        <input type="password" id="password" name="password" value="" class="form-control"
                            placeholder="Enter New Password" />

                    </div>
                    <div class="form-group">
                        <label for="inputSubject">Confirm-Password</label>
                        <input type="password" id="pass_confirmation" name="pass_confirmation" value=""
                            class="form-control" placeholder="Enter Confirm Password" />
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" type="submit">Update</button>
                    </div>
                </form>
            </div>

        </div>
    </div>

</section>
<!-- /.content -->
@endsection
@push('js')
<!-- For success msg -->
<script src="{{asset('Backend/plugins/toastr/toastr.min.js')}}"></script>
@if(session()->has('success'))
<script>
     toastr.success("{!! session('success') !!}");
</script>
@endif
@endpush
