<script src="{{asset('Backend/plugins/toastr/toastr.min.js')}}"></script>
@if(session()->has('success'))
<script>
     toastr.success("{!! session('success') !!}");
</script>
@endif

@if(session()->has('update'))
<script>
     toastr.update("{!! session('update') !!}");
</script>
@endif

@if(session()->has('errormessage'))
<script>
     toastr.errormessage("{!! session('errormessage') !!}");
</script>
@endif

