@extends('Training.dashboard.layouts.app')
@push('css')
<link rel="stylesheet" href="{{ asset('backend/assets/summernote/summernote-bs4.min.css') }}">

@endpush
@section('content')
<div class="page-content">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                    <h4 class="mb-sm-0 font-size-18">Trainee</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Trainee</a></li>
                            <li class="breadcrumb-item active">Create</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                <form action="{{route('training-trainee.update',$data->id)}}" method="post" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="card-body">

                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Full Name</label>
                                <div class="col-md-3">
                                    <input class="form-control" name="name" type="text" value="{{$data->name}}" placeholder="Full Name" id="example-text-input">
                                </div>
                                <label for="example-search-input" class="col-md-2 col-form-label">पुरा शीर्षक</label>
                                <div class="col-md-3">
                                    <input class="form-control" name="name_np" type="text" value="{{$data->name_np}}" placeholder="पुरा शीर्षक" id="nepali">
                                </div>
                            </div>

                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">User Name</label>
                                <div class="col-md-3">
                                    <input class="form-control" name="email" type="text" value="{{$data->email}}" id="example-text-input">
                                </div>
                                <label for="example-search-input" class="col-md-2 col-form-label">Password</label>
                                <div class="col-md-3">
                                    <input class="form-control" name="password" type="text" placeholder="Enter New Password" id="nepali">
                                </div>
                            </div>

                            <div class="mb-3 row">
                                <label for="example-email-input" class="col-md-2 col-form-label">Image</label>
                                <div class="col-md-3">
                                <input class="form-control" name="image" type="file" onchange="readURL(this)" placeholder="" id="example-email-input">
                                    <img src="{{asset('/Admin/files/TrainingStaff/'.$data->image) }}" width="200px"style="padding-top:10px" id="one">
                                </div>
                            </div>

                            <div class="mb-3 row">
                                <label for="example-search-input" class="col-md-2 col-form-label">Department</label>
                                <div class="col-md-3">
                                    <select name="department_id" class="form-control" id="example-search-input">
                                        @if($department)
                                        @foreach($department as $department)
                                        <option value="{{$department->id}}" {{$data->department_id ==
                                        $department->id ? 'selected' : ''}}>{{$department->training_department}}
                                        </option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="card-body">
                            <div class="col-sm-6">
                                <div class="mt-4">
                                    <div>
                                        <div class="input-group">
                                            <button class="btn btn-primary" type="submit" id="inputGroupFileAddon04">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div> <!-- end col -->
        </div>
        <!-- end row -->

    </div> <!-- container-fluid -->
</div>
<!-- End Page-content -->
@endsection
@push('js')
<script src="https://unpkg.com/nepalify"></script>
<script>
    nepalify.interceptElementById("nepali");
</script>
<!-- jQuery -->
<script src="{{ asset('backend/assets/summernote/summernote-bs4.min.js') }}"></script>
<script>
    $(function() {
        // Initialize Summernote for the first editor with the "Italic" tool
        $('.summernote').summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['link', 'picture', 'video']],
                ['misc', ['codeview', 'fullscreen']]
            ],
            tabDisable: false // Enable tab key functionality
        });

        // Initialize CodeMirror for the first textarea
        CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
            mode: "htmlmixed",
            theme: "monokai"
        });
    });

    $(function() {
        // Initialize Summernote for the second editor with the "Italic" tool
        $('.summernote_two').summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['link', 'picture', 'video']],
                ['misc', ['codeview', 'fullscreen']]
            ],
            tabDisable: false // Enable tab key functionality
        });

        // Initialize CodeMirror for the second textarea
        CodeMirror.fromTextArea(document.getElementById("codeMirrorDemoTwo"), {
            mode: "htmlmixed",
            theme: "monokai"
        });
    });
</script>
<script>
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $("#one").attr('src', e.target.result).width(300)
        };
        reader.readAsDataURL(input.files[0]);
    }
}
</script>
@endpush
