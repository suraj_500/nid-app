@extends('Training.dashboard.layouts.app')
@push('css')
<link rel="stylesheet" href="{{ asset('backend/assets/summernote/summernote-bs4.min.css') }}">

@endpush
@section('content')
<div class="page-content">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                    <h4 class="mb-sm-0 font-size-18">Trainee</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Trainee</a></li>
                            <li class="breadcrumb-item active">Create</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <form action="{{route('training-trainee.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">

                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Full Name</label>
                                <div class="col-md-3">
                                    <input class="form-control" name="name" type="text" placeholder="Full Name" id="example-text-input">
                                </div>
                                <label for="example-search-input" class="col-md-2 col-form-label">पुरा शीर्षक</label>
                                <div class="col-md-3">
                                    <input class="form-control" name="name_np" type="text" placeholder="पुरा शीर्षक" id="nepali">
                                </div>
                            </div>

                            <div class="mb-3 row">
                            <label for="example-text-input" class="col-md-2 col-form-label">User Name</label>
                                <div class="col-md-3">
                                    <input class="form-control" name="email" type="text" placeholder="Full Name" id="example-text-input">
                                </div>
                                <label for="example-search-input" class="col-md-2 col-form-label">Password</label>
                                <div class="col-md-3">
                                    <input class="form-control" name="password" type="text" placeholder="Password" id="nepali">
                                </div>
                            </div>

                            <div class="mb-3 row">
                                <label for="example-email-input" class="col-md-2 col-form-label">Image</label>
                                <div class="col-md-3">
                                    <input class="form-control" name="image" type="file" value="" placeholder="" id="example-email-input">
                                </div>
                            </div>

                            <div class="mb-3 row">
                                <label for="example-search-input" class="col-md-2 col-form-label">Role</label>
                                <div class="col-md-3">
                                    <select name="role_id" class="form-control" id="example-search-input">
                                        @if($role)
                                        @foreach($role as $role)
                                        <option value="{{$role->id}}">{{$role->training_role}}
                                        </option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                <label for="example-search-input" class="col-md-2 col-form-label">Department</label>
                                <div class="col-md-3">
                                    <select name="department_id" class="form-control" id="example-search-input">
                                        @if($department)
                                        @foreach($department as $department)
                                        <option value="{{$department->id}}">{{$department->training_department}}
                                        </option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="mb-3 row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Login Expiry Date</label>
                                <div class="col-md-3">
                                    <input class="form-control" name="expiry_date" type="date" placeholder="" id="example-text-input">
                                </div>
                            </div>

                        </div>

                        <div class="card-body">
                            <div class="col-sm-6">
                                <div class="mt-4">
                                    <div>
                                        <div class="input-group">
                                            <button class="btn btn-primary" type="submit" id="inputGroupFileAddon04">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div> <!-- end col -->
        </div>
        <!-- end row -->

    </div> <!-- container-fluid -->
</div>
<!-- End Page-content -->
@endsection
@push('js')
<script src="https://unpkg.com/nepalify"></script>
<script>
    nepalify.interceptElementById("nepali");
</script>
<!-- jQuery -->
@endpush
