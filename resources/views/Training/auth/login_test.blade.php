<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>

    <link rel="stylesheet" href="./css/main.css" />
    <link rel="stylesheet" href="{{ asset('Frontend/css/package/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('Frontend/css/package/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('Frontend/css/package/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('Frontend/css/package/font.css') }}">
    <link rel="stylesheet" href="{{ asset('Frontend/css/package/aos.css') }}">
    <link rel="stylesheet" href="{{ asset('Frontend/css/variables.css') }}">
    <link rel="stylesheet" href="{{ asset('Frontend/css/global.css') }}">
    <link rel="stylesheet" href="{{ asset('Frontend/css/style.css') }}">
</head>

<body>
    <div class="login">
        <div class="card col-xl-9 col-11 mx-auto">
            <div class="row align-items-center">
                <div class="col-xl-6 col-12 card-one">
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <img src="{{ asset('Frontend/asset/icons/login-logo.svg') }}" alt="logo" class="logo">
                            <h4 class="text-grey-900 fw-700 text-center mb-1">Training Detail</h4>
                            <h6 class="hfs7 fw-300 text-color-1000 text-center">The organization's role is to furnish
                                the Nepali
                                government
                                with the necessary
                                information to
                                advance the state's interests, including its independence, sovereignty, and security.
                            </h6>
                        </div>
                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <img src="{{ asset('Frontend/asset/icons/login-logo.svg') }}" alt="logo" class="logo">
                            <h4 class="text-grey-900 fw-700 text-center mb-1">Trainee Detail</h4>
                            <h6 class="hfs7 fw-300 text-color-1000 text-center">The organization's role is to furnish
                                the Nepali
                                government
                                with the necessary
                                information to
                                advance the state's interests, including its independence, sovereignty, and security.
                            </h6>
                        </div>
                        <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">...</div>
                    </div>
                </div>
                <div class="col-xl-6 col-12 card-two">
                    <div class="d-flex gap-2 mb-48">
                        <img src="{{ asset('Frontend/asset/images/logo.svg') }}" alt="logo" width="70" height="58">
                        <div>
                            <h5 class="text-secondary fw-700 mb-0">National Investigation Department</h5>
                            <h5 class="text-secondary fw-700 mb-0">राष्ट्रिय अनुसन्धान बिभाग</h5>
                        </div>
                    </div>
                    <div class="mb-52 ">
                        <h3 class="fw-600 text-main">Choose Program</h3>
                        <h6 class="fw-400 text-gray-700">Welcome to National Investigation Department</h6>
                    </div>
                    <ul class="nav nav-pills mb-3 gap-4 justify-content-center" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">
                                <img src="{{ asset('Frontend/asset/icons/trainer.svg') }}" alt="trainer" height="130" width="130" class="mb-12">
                                <h6 class="fw-500 mb-12 text-primary-900 mb-0">Trainer</h6>
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">
                                <img src="{{ asset('Frontend/asset/icons/trainee.svg') }}" alt="trainee" height="130" width="130" class="mb-12">
                                <h6 class="fw-500 mb-12 text-primary-900 mb-0">Trainee</h6>
                            </button>
                        </li>
                    </ul>
                    <a href="{{ route('training.login.email') }}" class="btn btn-login w-100">
                        Login
                    </a>
                </div>
            </div>
        </div>
    </div>
</body>

<script src="{{ asset('Frontend/js/package/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('Frontend/js/package/jquery.js') }}"></script>
<script src="{{ asset('Frontend/js/package/slick.min.js') }}"></script>
<script src="{{ asset('Frontend/js/package/aos.js') }}"></script>
<script src="{{ asset('Frontend/js/script.js') }}"></script>


</html>
