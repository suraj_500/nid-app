<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Training | Log in </title>
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('Auth/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{asset('Auth/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('Auth/dist/css/adminlte.min.css')}}">
</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <!-- /.login-logo -->
        <div class="card card-outline card-primary">
            <div class="card-header text-center">
                <a href="../../index2.html" class="h1"><b>Training </b>Login</a>
            </div>
            <div class="card-body">
                <p class="login-box-msg">Sign in to start your Training session</p>
                <!-- {{-- @if(Session::has('info_message'))
                @include('backend.admin.config.notify')
                @endif --}}

                {{-- @include('backend.admin.config.notify') --}} -->
                @include('Admin.admin.auth.notify')
                <form action="{{route('logintraining')}}" method="post">
                    @csrf
                    <div class="input">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Email" name="email" autocomplete="off">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                </div>
                            </div>
                        </div>
                        <span style="color: red">@error('email'){{$message}}@enderror</span>
                    </div>

                    <div class="input">
                        <div class="input-group mb-3">
                            <input type="password" class="form-control" placeholder="Password" name="password">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                        <span style="color: red">@error('email'){{$message}}@enderror</span>
                    </div>
                    <div class="row">
                        <!-- {{-- <div class="col-8">
                            <div class="icheck-primary">
                                <input type="checkbox" id="remember">
                                <label for="remember">
                                    Remember Me
                                </label>
                            </div>
                        </div> --}} -->
                        <!-- /.col -->
                        <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.login-box -->
    <footer>

        <!-- jQuery -->
        <script src="{{asset('Auth/plugins/jquery/jquery.min.js')}}"></script>
        <!-- Bootstrap 4 -->
        <script src="{{asset('Auth/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <!-- AdminLTE App -->
        <script src="{{asset('Auth/dist/js/adminlte.min.js')}}"></script>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
        </script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
        </script>

        <script type="text/javascript">
            $("document").ready(function() {
                setTimeout(function() {
                    $("div.alert").remove();
                }, 3000);
            });
        </script>

        <!-- For password -->
        <script>
            const togglePassword = document.querySelector("#togglePassword");
            const password = document.querySelector("#password");
            togglePassword.addEventListener("click", function() {
                // toggle the type attribute
                const type = password.getAttribute("type") === "password" ? "text" : "password";
                password.setAttribute("type", type);
                // toggle the icon
                this.classList.toggle("fa-eye");
                this.classList.toggle("fa-eye-slash");
            });
        </script>

        <!-- For confirm password -->
        <script>
            const toggleConfirmPassword = document.querySelector("#toggleConfirmPassword");
            const confirmpassword = document.querySelector("#confirmpassword");
            toggleConfirmPassword.addEventListener("click", function() {
                // toggle the type attribute
                const type = confirmpassword.getAttribute("type") === "password" ? "text" : "password";
                confirmpassword.setAttribute("type", type);
                // toggle the icon
                this.classList.toggle("fa-eye");
                this.classList.toggle("fa-eye-slash");
            });
        </script>

    </footer>

</html>
