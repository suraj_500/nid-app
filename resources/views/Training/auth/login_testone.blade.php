<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>

    <link rel="stylesheet" href="./css/main.css" />
    <link rel="stylesheet" href="{{ asset('Frontend/css/package/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('Frontend/css/package/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('Frontend/css/package/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('Frontend/css/package/font.css') }}">
    <link rel="stylesheet" href="{{ asset('Frontend/css/package/aos.css') }}">
    <link rel="stylesheet" href="{{ asset('Frontend/css/variables.css') }}">
    <link rel="stylesheet" href="{{ asset('Frontend/css/global.css') }}">
    <link rel="stylesheet" href="{{ asset('Frontend/css/style.css') }}">
</head>

<body>
    <div class="login">
        <div class="card col-xl-9 col-11 mx-auto">
            <div class="row align-items-center">
                <div class="col-xl-6 col-12 card-one">
                    <img src="{{ asset('Frontend/asset/icons/login-logo.svg') }}" alt="logo" class="logo"
                        width="100%">
                    <h4 class="text-grey-900 fw-700 text-center mb-1">Trainer Detail</h4>
                    <h6 class="hfs7 fw-300 text-color-1000 text-center">The organization's role is to furnish
                        the Nepali
                        government
                        with the necessary
                        information to
                        advance the state's interests, including its independence, sovereignty, and security.
                    </h6>
                </div>
                <div class="col-xl-6 col-12 card-two">
                    <div class="d-flex gap-2 mb-48">
                        <img src="{{ asset('Frontend/asset/images/logo.svg') }}" alt="logo" width="70"
                            height="58">
                        <div>
                            <h5 class="text-secondary fw-700 mb-0">National Investigation Department</h5>
                            <h5 class="text-secondary fw-700 mb-0">राष्ट्रिय अनुसन्धान बिभाग</h5>
                        </div>
                    </div>
                    <div class="mb-52 ">
                        <h3 class="fw-600 text-main">Login</h3>
                        <h6 class="fw-400 text-gray-700">Welcome to National Investigation Department</h6>
                    </div>
                    <form action="{{ route('training.login.email.submit') }}" method="post">
                        @csrf
                        <div class="">
                            <label for="" class="mb-2">Email<span class="text-secondary ms-1">*</span></label>
                            <input type="email" name="email" class="form-control" id=""
                                placeholder=" Write Email ">
                                <span style="color: red">@error('email'){{ $message }}@enderror</span>
                        </div>
                        <button type="submit" class="btn btn-login w-100">
                            Next
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

<script src="{{ asset('Frontend/js/package/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('Frontend/js/package/jquery.js') }}"></script>
<script src="{{ asset('Frontend/js/package/slick.min.js') }}"></script>
<script src="{{ asset('Frontend/js/package/aos.js') }}"></script>
<script src="{{ asset('Frontend/js/script.js') }}"></script>


</html>
