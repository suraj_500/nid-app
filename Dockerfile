FROM node:18-alpine AS npm
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build
# Composer stage for PHP dependencies
FROM composer AS build
WORKDIR /app
COPY --from=npm /app /app
RUN rm composer.lock && composer install \
  --optimize-autoloader \
  --no-interaction \
  --no-progress \
  --ignore-platform-reqs
RUN composer dumpautoload
# PHP stage
FROM php:8.2-fpm-alpine
WORKDIR /app
# Install necessary dependencies
RUN apk update && \
    apk add --no-cache \
        libzip \
        libzip-dev \
        zlib-dev \
        libpng-dev \
        libjpeg-turbo-dev \
        zip\
        freetype-dev
# Install PHP extensions with libzip
RUN docker-php-ext-configure gd --with-freetype --with-jpeg && \
    docker-php-ext-install gd pdo pdo_mysql zip
COPY --from=build /app /app
CMD [ "/bin/sh", "artisan.sh" ]
