
function initializeSlider() {
    $('.banner__slider').slick({
        dots: true,
        autoplay: true,
        infinite: true,
        speed: 100,
        slidesToShow: 1,
        autoplaySpeed: 5000,
        arrows: false,
    });
    $('.notice-slider').slick({
        dots: true,
        autoplay: true,
        infinite: true,
        speed: 100,
        slidesToShow: 1,
        autoplaySpeed: 5000,
        arrows: false,
    });
    $('.news__slider').slick({
        dots: true,
        infinite: true,
        autoplay: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
}

const scrollToTop = () => {
    let mybutton = document.getElementById("myBtn");
    mybutton.addEventListener('click', function () {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    })
}

const animation = () => {
    AOS.init({
        duration: 1500,
        once: true,
    });
}
const functions = [initializeSlider, scrollToTop, animation];

for (let i = 0; i < functions.length; i++) {
    try {
        functions[i]();
    } catch (error) { }
}
