<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;

class CrudService
{
    public function create($modelName, $data)
    {
        $modelClass = "App\\Models\\$modelName";

        if (!class_exists($modelClass)) {
            throw new \InvalidArgumentException("Model class $modelClass not found.");
        }
        if (isset($data['title'])) {
            $data['slug'] = $this->generateSlug($data['title'], $modelClass);
        }
        if (isset($data['image'])) {
            $data['image'] = $this->uploadImage($data['image']);
        }

        return $modelClass::create($data);
    }

    public function update($modelName, $id, $data)
    {
        $modelClass = "App\\Models\\$modelName";
        $model = $modelClass::findOrFail($id);

        if (isset($data['image'])) {
            $this->deleteImageIfExists($model->image);
            $data['image'] = $this->uploadImage($data['image']);
        }

        $model->fill($data)->save();

        return $model;
    }

    public function delete($modelName, $id)
    {
        $modelClass = "App\\Models\\$modelName";
        $model = $modelClass::findOrFail($id);
        if ($model->image) {
            $this->deleteImageIfExists($model->image);
        }
        $model->delete();

        return true;
    }

    public function find($modelName, $id)
    {
        $modelClass = "App\\Models\\$modelName";

        return $modelClass::findOrFail($id);
    }

    public function all($modelName)
    {
        $modelClass = "App\\Models\\$modelName";

        return $modelClass::latest()->get();
    }

    protected function uploadImage($image)
    {
        $destinationPath = 'uploads/images/';
        $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
        $image->move($destinationPath, $profileImage);
        $imageName = "$profileImage";
        return $imageName;
    }

    protected function deleteImageIfExists($imageName)
    {
        if ($imageName) {
            $imagePath = 'uploads/images/' . $imageName;
            if (file_exists($imagePath)) {
                unlink($imagePath);
            }
        }
    }

    private function generateSlug($title, $modelClass)
    {
        // Remove special characters
        $slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $title);

        // Convert to lowercase
        $slug = strtolower($slug);

        // Remove leading/trailing dashes
        $slug = trim($slug, '-');

        // Generate unique slug
        $count = 1;
        $originalSlug = $slug;
        while ($this->slugExists($slug, $modelClass)) {
            $slug = $originalSlug . '-' . $count;
            $count++;
        }

        return $slug;
    }

    private function slugExists($slug, $modelClass)
{

    if (!class_exists($modelClass)) {
        throw new \InvalidArgumentException("Model class $modelClass not found.");
    }
    $model = new $modelClass;

    return $model->where('slug', $slug)->exists();
}
}
