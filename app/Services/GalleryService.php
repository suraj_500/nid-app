<?php

namespace App\Services;

use App\Models\Gallery\Gallery;
use App\Models\Gallery\GalleryImage;
use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;

class GalleryService
{
    public function store($data)
    {
        // DB::beginTransaction();
        // try {
        // Create the Gallery entry
        $gallery = Gallery::create([
            'title' => $data['title'],
            'title_np' => $data['title_np'],
            'description' => $data['description'],
            'description_np' => $data['description_np'],
            'slug' => Str::slug($data['title']),
            'status' => $data['status'] ?? 1,
        ]);

        if (isset($data['image']) && is_array($data['image'])) {
            foreach ($data['image'] as $imageFile) {
                if ($imageFile instanceof UploadedFile) {
                    \Log::info('Storing image: ' . $imageFile->getClientOriginalName());
                    $this->storeImage($gallery->id, $imageFile);
                } else {
                    \Log::info('Not an instance of UploadedFile');
                }
            }
        } else {
            \Log::info('No images or not an array');
        }

        // DB::commit();
        // } catch (\Exception $e) {
        //     DB::rollBack();
        //     throw $e;
        // }
    }


    public function update($data, $id)
    {
        $gallery = Gallery::findOrFail($id);
        $gallery->update([
            'title' => $data['title'],
            'title_np' => $data['title_np'],
            'description' => $data['description'],
            'description_np' => $data['description_np'],
            'slug' => Str::slug($data['title']),
            'status' => $data['status'] ?? 1,
        ]);

        if (isset($data['image']) && is_array($data['image'])) {
            foreach ($data['image'] as $imageFile) {
                $this->storeImage($gallery->id, $imageFile);
            }
        }
    }

    protected function storeImage($galleryId, UploadedFile $imageFile)
    {
        $directory = 'Admin/files/Gallery';
        $fileName = $this->uploadFile($imageFile, $directory);

        GalleryImage::create([
            'image_id' => $galleryId,
            'image' => $directory . '/' . $fileName,
        ]);
    }


    private function uploadFile(UploadedFile $file, $directory)
    {
        $destinationPath = public_path($directory);

        if (!file_exists($destinationPath)) {
            mkdir($destinationPath, 0755, true);
        }

        // Generate a unique filename
        $fileName = date('YmdHis') . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
        $file->move($destinationPath, $fileName);

        return $fileName;
    }
}
