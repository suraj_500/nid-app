<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;

class GenericService
{
    public function store(Model $model, array $data, UploadedFile $file = null, $fileField = 'file', $slugField = 'slug', $fileTypeField = 'file_type'): void
    {
        $directory = 'Admin/files/' . class_basename($model);

        if ($file) {
            $fileName = $this->uploadFile($file, $directory);
            $data[$fileField] = $fileName;
            $data[$fileTypeField] = $this->getFileType($file);
        }

        if (empty($data[$slugField])) {
            $data[$slugField] = $this->generateSlug($data);
        }

        $model::create($data);
    }

    private function generateSlug(array $data): string
    {
        if (!empty($data['name'])) {
            return Str::slug($data['name']);
        } elseif (!empty($data['title'])) {
            return Str::slug($data['title']);
        }
        return '';
    }

    public function update(Model $model, array $data, $id, UploadedFile $file = null, $fileField = 'file', $slugField = 'slug', $fileTypeField = 'file_type'): bool
    {
        $instance = $model::find($id);
        if (!$instance) {
            return false;
        }

        $directory = 'Admin/files/' . class_basename($model);

        if ($file) {
            $fileName = $this->uploadFile($file, $directory);
            $data[$fileField] = $fileName;
            $data[$fileTypeField] = $this->getFileType($file);

            // Delete old file
            $oldFilePath = public_path($directory . '/' . $instance->$fileField);
            if (file_exists($oldFilePath)) {
                unlink($oldFilePath);
            }
        }

        if (isset($data[$slugField])) {
            $data[$slugField] = Str::slug($data[$slugField]);
        }

        $instance->update($data);
        return true;
    }

    private function uploadFile(UploadedFile $file, $directory)
    {
        $destinationPath = public_path($directory);

        if (!file_exists($destinationPath)) {
            mkdir($destinationPath, 0755, true);
        }

        $fileName = date('YmdHis') . '.' . $file->getClientOriginalExtension();
        $file->move($destinationPath, $fileName);

        return $fileName;
    }

    private function getFileType(UploadedFile $file): string
    {
        $mimeType = $file->getClientMimeType();

        switch ($mimeType) {
            case 'image/jpeg':
            case 'image/png':
            case 'image/gif':
                return 'image';
            case 'application/pdf':
                return 'pdf';
            case 'application/msword':
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                return 'word';
            case 'application/vnd.ms-excel':
            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                return 'excel';
            default:
                return 'unknown';
        }
    }
}
