<?php

namespace App\Models\ReportInformation;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportInformation extends Model
{
    use HasFactory;
    protected $fillable=['full_name','phone','email','file','subject'];
}
