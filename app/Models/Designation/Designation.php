<?php

namespace App\Models\Designation;

use App\Models\Staff\Staff;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    use HasFactory;
    protected $fillable=['name','name_np','slug','status'];
    public function staffs()
    {
        return $this->hasMany(Staff::class,'designation_id');
    }
}
