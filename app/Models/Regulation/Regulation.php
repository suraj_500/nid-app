<?php

namespace App\Models\Regulation;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Regulation extends Model
{
    use HasFactory;
    protected $fillable=['title','title_np','description','description_np','image','slug','type','status','category_id'];

    public function regulationcategory()
    {
        return $this->belongsTo(RegulationCategory::class,'category_id');
    }

}
