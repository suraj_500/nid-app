<?php

namespace App\Models\Regulation;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RegulationCategory extends Model
{
    use HasFactory;
    protected $fillable=['title','title_np','slug','status'];

    public function regulations()
    {
        return $this->hasMany(Regulation::class,'category_id');
    }
}
