<?php

namespace App\Models\HomePage;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BannerNotice extends Model
{
    use HasFactory;
    protected $fillable=['title','title_np','status'];
}
