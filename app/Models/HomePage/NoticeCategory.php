<?php

namespace App\Models\HomePage;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NoticeCategory extends Model
{
    use HasFactory;
    protected $fillable=['title','title_np','slug','status'];


    public function notices()
    {
        return $this->hasMany(Notice::class,'category_id');
    }
}
