<?php

namespace App\Models\HomePage;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
    use HasFactory;
    protected $fillable=['title','title_np','description','description_np','image','slug','status','category_id','type'];

    public function noticecategory()
    {
        return $this->belongsTo(NoticeCategory::class,'category_id');
    }
}
