<?php

namespace App\Models\Tender;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TenderCategory extends Model
{
    use HasFactory;
    protected $fillable=['title','title_np','slug','status'];


    public function tenders()
    {
        return $this->hasMany(Tender::class,'category_id');
    }
}
