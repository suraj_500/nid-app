<?php

namespace App\Models\Tender;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tender extends Model
{
    use HasFactory;
    protected $fillable=['title','title_np','description','description_np','image','slug','status','category_id','type'];

    public function tendercategory()
    {
        return $this->belongsTo(TenderCategory::class,'category_id');
    }
}
