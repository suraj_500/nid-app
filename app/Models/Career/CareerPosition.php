<?php

namespace App\Models\Career;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CareerPosition extends Model
{
    use HasFactory;

    protected $fillable=['name','name_np','slug','status'];


    public function careers()
    {
        return $this->hasMany(Career::class,'position_id');
    }
}
