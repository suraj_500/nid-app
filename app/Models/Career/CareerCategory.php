<?php

namespace App\Models\Career;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CareerCategory extends Model
{
    use HasFactory;

    protected $fillable=['title','title_np','slug','status'];

    public function careers()
    {
        return $this->hasMany(Career::class,'category_id');
    }
}
