<?php

namespace App\Models\Career;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    use HasFactory;

    protected $fillable=['title','title_np','description','description_np','image','slug','status','category_id','position_id','type'];

    public function careercategory()
    {
        return $this->belongsTo(CareerCategory::class,'category_id');
    }

    public function positioncategory()
    {
        return $this->belongsTo(CareerPosition::class,'position_id');
    }
}
