<?php

namespace App\Models\Staff;

use App\Models\Designation\Designation;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    use HasFactory;
    protected $fillable=['name','name_np','description','description_np','image','slug','status','designation_id','department_id'];

    public function designation()
    {
        return $this->belongsTo(Designation::class,'designation_id');
    }
}
