<?php

namespace App\Models\Training;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\RolesAndPermission\TrainingRole;
use App\Models\RolesAndPermission\TrainingDepartment;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class TrainingStaff extends Authenticatable
{
    use HasFactory;
    protected $guard='training';

    protected $fillable=['name','name_np','email','password','image','slug','status','role_id','department_id','expiry_date'];

    public function roles()
    {
        return $this->belongsTo(TrainingRole::class,'role_id');
    }

    public function departments()
    {
        return $this->belongsTo(TrainingDepartment::class,'department_id');
    }
}
