<?php

namespace App\Models\Training\Material;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrainingMaterial extends Model
{
    use HasFactory;
    protected $fillable=['title','title_np','description','description_np','image','slug','type','status','department_id'];
}
