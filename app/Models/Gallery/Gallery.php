<?php

namespace App\Models\Gallery;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use File;

class Gallery extends Model
{
    use HasFactory;
    protected $fillable=['title','title_np','description','description_np','slug','status'];

    public function galleryImages()
    {
        return $this->hasMany(GalleryImage::class, 'image_id');
    }

    public function deleteImage() {
        if(file_exists('Admin/files/Gallery/'.basename($this->image))){
            file::delete('Admin/files/Gallery/'.basename($this->image));
        }
    }

}
