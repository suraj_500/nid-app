<?php

namespace App\Models\Gallery;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GalleryImage extends Model
{
    use HasFactory;
    protected $fillable=['image','image_id'];

    public function gallery()
    {
        return $this->belongsTo(Gallery::class, 'image_id');
    }

    public function deleteImage() {
        if(file_exists('Admin/files/Gallery/'.basename($this->image))){
            unlink('Admin/files/Gallery/'.basename($this->image));
        }
    }
}
