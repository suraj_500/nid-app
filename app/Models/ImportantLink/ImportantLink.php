<?php

namespace App\Models\ImportantLink;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImportantLink extends Model
{
    use HasFactory;
    protected $fillable=['title','title_np','url','status'];
}
