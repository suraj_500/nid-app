<?php

namespace App\Models\PopUpNotice;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PopUpNotice extends Model
{
    use HasFactory;
    protected $fillable=['title','title_np','image','slug','status'];
}
