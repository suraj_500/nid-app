<?php

namespace App\Models\RolesAndPermission;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrainingDepartment extends Model
{
    use HasFactory;
    protected $fillable=['training_department','training_department_np','slug','status'];
}
