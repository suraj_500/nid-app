<?php

namespace App\Models\RolesAndPermission;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrainingRole extends Model
{
    use HasFactory;
    protected $fillable=['training_role','training_role_np','slug','status'];
}
