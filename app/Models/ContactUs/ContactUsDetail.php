<?php

namespace App\Models\ContactUs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactUsDetail extends Model
{
    use HasFactory;
    protected $fillable=['location','location_np','phone','phone_np','status'];
}
