<?php

use App\Models\Configuration\AboutUs;
use App\Models\Configuration\Configuration;
use App\Models\HomePage\BannerNotice;
use App\Models\ImportantLink\ImportantLink;

function getNotice()
{
    $notice = BannerNotice::orderby('id', 'desc')->get();
    return $notice;
}

function getConfiguration($key)
{
    $config = Configuration::where('configuration_key', '=', $key)->first();
    if ($config != null) {
        return $config->configuration_value;
    }
    return null;
}

function getAboutus($key)
{
    $config = AboutUs::where('configuration_key', '=', $key)->first();
    if ($config != null) {
        return $config->configuration_value;
    }
    return null;
}

function getImportantlinks()
{
    $links = ImportantLink::select('title', 'url')->get();
    return $links;
}

function getImportantlinkslimit()
{
    $links = ImportantLink::select('title', 'url')->limit(6)->get();
    return $links;
}
