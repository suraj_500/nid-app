<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Auth\Admin;
use Auth;
use Str;
use File;
use Session;

class AdminProfileController extends Controller
{
    public function getChangeProfile(){
    	$adminDetails=Admin::where('email',Auth::guard('admin')->user()->email)->first();
    	return view('Admin.admin.profile.profile_update',compact('adminDetails'));
    }
    public function postChangeProfile(Request $request,$id){
    	$datas=$request->all();
    	$admin = new Admin();
        $admin = $admin->find($id);

        // $rules=$admin->getRules();
        // $request->validate($rules);
        if ($request->file('image')) {
            $file_name = "Image-" . date('ymdhis') . rand(0, 999) . "." . $request->image->getClientOriginalExtension();
            $path = public_path() . '/Admin/Profile';
            if (!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $success = $request->image->move($path, $file_name);
            if ($success) {
                $datas['image'] = $file_name;
            } else {
                $datas['image'] = null;
            }
            $destination = 'Admin/Profile/' . $admin->image;
            if (file::exists($destination)) {
                file::delete($destination);
            }
        }
        $admin->fill($datas);
        $status = $admin->update();
        Session::flash('success', 'Data Updated Succesfully');
    	return redirect()->route('admin-dashboard')->with('success','Your Profile has been changed successfully.');
    }
}
