<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\AdminLoginRequest;
use App\Models\Auth\Admin;
use Auth;
use Session;

class AdminloginController extends Controller
{
    public function adminLogin(){
    	return view('Admin.admin.auth.login');
    }

    public function postAdminLogin(AdminLoginRequest $request){
    	$admin = new Admin();
    	$data=$request->all();
    	if(Auth::guard('admin')->attempt(['email'=>$data['email'],'password'=>$data['password']])){
    		return redirect('dashboard');
    	}else{
    		return redirect()->back()->with('errormessage', 'Authentication Exception Occured!');
    	}
    }

    public function adminLogout(){
    	Auth::guard('admin')->logout();
    	Session::flash('info_message', 'Logout Succesfully');
    	return redirect('/admin/login');
    }
}
