<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Auth\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Session;
use Hash;

class AdminPasswordController extends Controller
{
    public function getChangePassword(){
    	$user = Admin::where('email', Auth::guard('admin')->user()->email)->first();
    	return view('Admin.admin.profile.password_update', compact('user'));
    }
    public function postChangePassword(Request $request, $id){
    	$data = $request->all();
        $validationData = $request->validate([
            'current_password' => 'required|max:255',
            'password' => 'min:6',
            'pass_confirmation' => 'required_with:password|same:password|min:6'
        ]);
        dd($validationData);
        $user = Admin::where('email', Auth::guard('admin')->user()->email)->first();
        $current_user_password = $user->password;
        if (Hash::check($data['current_password'], $current_user_password)) {
            $user->password = bcrypt($data['password']);
            $user->save();
            Session::flash('success', 'Your passord has been changed.');
            return redirect()->route('admin-dashboard')->with('success','Your Password has been changed successfully.');
        } else {
            Session::flash('success', 'Your Current Password doesnot match.');
            return redirect()->route('change/password')->with('success', 'Your Current Password doesnot match.');
        }
    }
}
