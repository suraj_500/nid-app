<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Tender\Tender;
use App\Models\Tender\TenderCategory;
use Illuminate\Http\Request;
use App\Services\GenericService;
use Yajra\DataTables\DataTables;

class TenderController extends Controller
{
    protected $genericService;

    public function __construct(GenericService $genericService)
    {
        $this->genericService = $genericService;
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Tender::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    $actionBtn = '<a title="Edit About-Us" href="/admin-tender/' . $data->id . '/edit" class="btn btn-sm btn-primary mr-2"><i class="fas fa-edit"></i></a>';
                    $actionBtn .= '<a title="Delete About-Us" class="btn btn-sm btn-danger" onclick="handleDelete(' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                    return $actionBtn;
                })
                ->addColumn('status', function ($data) {
                    return $data->status == '0' ? 'In-Active' : 'Active';
                })
                ->addColumn('image', function ($data) {
                    $imagePath = $data->image ? asset('Admin/images/Tender/'.$data->image) : asset('default_image/profile.png');
                    return '<a href="'.$imagePath.'" width="50" height="50" target="__blank" />View</a>';
                })
                ->rawColumns(['action','image'])
                ->make(true);
        }
        return view('dashboard.tender.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $category=TenderCategory::all();
        return view('dashboard.tender.create',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required',
            'title_np' => 'required',
        ]);

        $this->genericService->store(new Tender, $request->only(['title','title_np','description','description_np','slug','status','category_id','type']), $request->file('image'), 'image', 'title','type');
        toast('Data Stored!', 'success');
        return redirect()->route('admin-tender.index')->with('success', 'Data created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = Tender::findOrFail($id);
        $category=TenderCategory::all();
        return view('dashboard.tender.edit', compact('data','category'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $validatedData = $request->validate([
            'title' => 'required',
            'title_np' => 'required',
        ]);

        $result = $this->genericService->update(new Tender, $request->only(['title','title_np','description','description_np','slug','status','category_id','type']), $id, $request->file('image'), 'image', 'title', 'Admin/files/Notice');

        if ($result) {
            toast('Data Updated!', 'success');
            return redirect()->route('admin-tender.index')->with('success', 'Data updated successfully.');
        } else {
            return redirect()->route('admin-tender.index')->with('error', 'Failed to update testimonial.');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = Tender::find($id);
        if (file_exists('Admin/files/Tender/' . basename($data->image))) {
            unlink('Admin/files/Tender/' . basename($data->image));
        }
        $data->delete();
        // Alert::success('Congrats', 'You\'ve Successfully Deleted Data');
        toast('Data Deleted!', 'success');
        return redirect()->route('admin-tender.index');
    }
}
