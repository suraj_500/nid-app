<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\PopUpNotice\PopUpNotice;
use Illuminate\Http\Request;
use App\Services\GenericService;
use Yajra\DataTables\DataTables;

class PopupnoticeController extends Controller
{
    protected $genericService;

    public function __construct(GenericService $genericService)
    {
        $this->genericService = $genericService;
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = PopUpNotice::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    $actionBtn = '<a title="Edit About-Us" href="/admin-pop-notice/' . $data->id . '/edit" class="btn btn-sm btn-primary mr-2"><i class="fas fa-edit"></i></a>';
                    $actionBtn .= '<a title="Delete About-Us" class="btn btn-sm btn-danger" onclick="handleDelete(' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                    return $actionBtn;
                })
                ->addColumn('status', function ($data) {
                    return $data->status == '0' ? 'In-Active' : 'Active';
                })
                ->addColumn('image', function ($data) {
                    $imagePath = $data->image ? asset('Admin/files/PopUpNotice/'.$data->image) : asset('default_image/profile.png');
                    return '<a href="'.$imagePath.'" width="50" height="50" target="__blank" />View</a>';
                })
                ->rawColumns(['action','image'])
                ->make(true);
        }
        return view('dashboard.pop_up_notice.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('dashboard.pop_up_notice.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required',
            'title_np' => 'required',
        ]);

        $this->genericService->store(new PopUpNotice, $request->only(['title','title_np','image','slug','status']), $request->file('image'), 'image','title', 'slug');
        toast('Data Stored!', 'success');
        return redirect()->route('admin-pop-notice.index')->with('success', 'Data created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = PopUpNotice::findOrFail($id);
        return view('dashboard.pop_up_notice.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $validatedData = $request->validate([
            'title' => 'required',
            'title_np' => 'required',
        ]);

        $result = $this->genericService->update(new PopUpNotice, $request->only(['title','title_np','image','slug','status']), $id, $request->file('image'), 'image', 'name', 'Admin/files/PopUpNotice');

        if ($result) {
            toast('Data Updated!', 'success');
            return redirect()->route('admin-pop-notice.index')->with('success', 'Data updated successfully.');
        } else {
            return redirect()->route('admin-pop-notice.index')->with('error', 'Failed to update testimonial.');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = PopUpNotice::find($id);
        if (file_exists('Admin/files/PopUpNotice/' . basename($data->image))) {
            unlink('Admin/files/PopUpNotice/' . basename($data->image));
        }
        $data->delete();
        // Alert::success('Congrats', 'You\'ve Successfully Deleted Data');
        toast('Data Deleted!', 'success');
        return redirect()->route('admin-pop-notice.index');
    }
}
