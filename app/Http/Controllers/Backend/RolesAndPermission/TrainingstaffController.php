<?php

namespace App\Http\Controllers\Backend\RolesAndPermission;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Training\TrainingStaff;
use App\Models\RolesAndPermission\TrainingRole;
use App\Models\RolesAndPermission\TrainingDepartment;
use App\Services\GenericService;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Hash;

class TrainingstaffController extends Controller
{
    protected $genericService;

    public function __construct(GenericService $genericService)
    {
        $this->genericService = $genericService;
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = TrainingStaff::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    $actionBtn = '<a title="Edit About-Us" href="/admin-trainer/' . $data->id . '/edit" class="btn btn-sm btn-primary mr-2"><i class="fas fa-edit"></i></a>';
                    $actionBtn .= '<a title="Delete About-Us" class="btn btn-sm btn-danger" onclick="handleDelete(' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                    return $actionBtn;
                })
                ->addColumn('status', function ($data) {
                    return $data->status == '0' ? 'In-Active' : 'Active';
                })
                ->addColumn('image', function ($data) {
                    $imagePath = $data->image ? asset('Admin/files/TrainingStaff/' . $data->image) : asset('default_image/profile.png');
                    return '<img src="' . $imagePath . '" width="50" height="50" />';
                })
                ->rawColumns(['action', 'image'])
                ->make(true);
        }
        return view('dashboard.training.staff.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $role = TrainingRole::where('id', 1)->get();
        return view('dashboard.training.staff.create', compact('role'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'name_np' => 'required',
            'password' => 'required', // Add validation rule for password
        ]);

        $data = $request->only(['name', 'name_np', 'email', 'image', 'slug', 'status', 'role_id']);
        $data['password'] = bcrypt($request->password); // Hash the password

        $this->genericService->store(new TrainingStaff, $data, $request->file('image'), 'image', 'slug');
        toast('Data Stored!', 'success');
        return redirect()->route('admin-trainer.index')->with('success', 'Data created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = TrainingStaff::findOrFail($id);
        return view('dashboard.training.staff.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'name_np' => 'required',
        ]);

        $data = $request->only(['name', 'name_np', 'email', 'password', 'image', 'slug', 'status', 'role_id']);

        if (!empty($data['password'])) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }

        $result = $this->genericService->update(new TrainingStaff, $data, $id, $request->file('image'), 'image', 'slug', 'Admin/files/TrainingStaff');

        if ($result) {
            toast('Data Updated!', 'success');
            return redirect()->route('admin-trainer.index')->with('success', 'Data updated successfully.');
        } else {
            return redirect()->route('admin-trainer.index')->with('error', 'Failed to update testimonial.');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = TrainingStaff::find($id);
        if (file_exists('Admin/files/TrainingStaff/' . basename($data->image))) {
            unlink('Admin/files/TrainingStaff/' . basename($data->image));
        }
        $data->delete();
        // Alert::success('Congrats', 'You\'ve Successfully Deleted Data');
        toast('Data Deleted!', 'success');
        return redirect()->route('admin-trainer.index');
    }
}
