<?php

namespace App\Http\Controllers\Backend\RolesAndPermission;

use App\Http\Controllers\Controller;
use App\Models\RolesAndPermission\TrainingDepartment;
use Illuminate\Http\Request;
use App\Services\GenericService;
use Yajra\DataTables\DataTables;

class TrainingdepartmentController extends Controller
{
    protected $genericService;

    public function __construct(GenericService $genericService)
    {
        $this->genericService = $genericService;
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = TrainingDepartment::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    $actionBtn = '<a title="Edit About-Us" href="/admin-training-department/' . $data->id . '/edit" class="btn btn-sm btn-primary mr-2"><i class="fas fa-edit"></i></a>';
                    $actionBtn .= '<a title="Delete About-Us" class="btn btn-sm btn-danger" onclick="handleDelete(' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                    return $actionBtn;
                })
                ->addColumn('status', function ($data) {
                    return $data->status == '0' ? 'In-Active' : 'Active';
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('dashboard.training.department.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('dashboard.training.department.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'training_department' => 'required',
            'training_department_np' => 'required',
        ]);

        $this->genericService->store(new TrainingDepartment, $request->only(['training_department','training_department_np','slug','status']));
        toast('Data Stored!', 'success');
        return redirect()->route('admin-training-department.index')->with('success', 'Data created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = TrainingDepartment::findOrFail($id);
        return view('dashboard.training.department.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'name_np' => 'required',
        ]);

        $result = $this->genericService->update(new TrainingDepartment, $request->only(['training_department','training_department_np','slug','status']), $id);

        if ($result) {
            toast('Data Updated!', 'success');
            return redirect()->route('admin-training-department.index')->with('success', 'Data updated successfully.');
        } else {
            return redirect()->route('admin-training-department.index')->with('error', 'Failed to update testimonial.');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = TrainingDepartment::find($id);
        $data->delete();
        toast('Data Deleted!', 'success');
        // Alert::success('Congrats', 'You\'ve Successfully Deleted Data');
        return redirect()->route('admin-training-department.index');
    }
}
