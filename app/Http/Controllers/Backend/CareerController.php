<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Career\Career;
use App\Models\Career\CareerCategory;
use App\Models\Career\CareerPosition;
use Illuminate\Http\Request;
use App\Services\GenericService;
use Yajra\DataTables\DataTables;

class CareerController extends Controller
{
    protected $genericService;

    public function __construct(GenericService $genericService)
    {
        $this->genericService = $genericService;
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Career::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    $actionBtn = '<a title="Edit About-Us" href="/admin-career/' . $data->id . '/edit" class="btn btn-sm btn-primary mr-2"><i class="fas fa-edit"></i></a>';
                    $actionBtn .= '<a title="Delete About-Us" class="btn btn-sm btn-danger" onclick="handleDelete(' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                    return $actionBtn;
                })
                ->addColumn('status', function ($data) {
                    return $data->status == '0' ? 'In-Active' : 'Active';
                })
                ->addColumn('image', function ($data) {
                    $imagePath = $data->image ? asset('Admin/files/Career/'.$data->image) : asset('default_image/profile.png');
                    return '<a href="'.$imagePath.'" width="50" height="50" target="__blank" />View</a>';
                })
                ->rawColumns(['action','image'])
                ->make(true);
        }
        return view('dashboard.career.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $category=CareerCategory::all();
        $position=CareerPosition::all();
        return view('dashboard.career.create',compact('category','position'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required',
            'title_np' => 'required',
        ]);

        $this->genericService->store(new Career, $request->only(['title','title_np','description','description_np','slug','status','category_id','type','position_id']), $request->file('image'), 'image', 'title','type');
        toast('Data Stored!', 'success');
        return redirect()->route('admin-career.index')->with('success', 'Data created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = Career::findOrFail($id);
        $category=CareerCategory::all();
        $position=CareerPosition::all();
        return view('dashboard.career.edit', compact('data','category','position'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $validatedData = $request->validate([
            'title' => 'required',
            'title_np' => 'required',
        ]);

        $result = $this->genericService->update(new Career, $request->only(['title','title_np','description','description_np','slug','status','category_id','position_id','type']), $id, $request->file('image'), 'image', 'title', 'Admin/files/Career');

        if ($result) {
            toast('Data Updated!', 'success');
            return redirect()->route('admin-career.index')->with('success', 'Data updated successfully.');
        } else {
            return redirect()->route('admin-career.index')->with('error', 'Failed to update testimonial.');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = Career::find($id);
        if (file_exists('Admin/files/Career/' . basename($data->image))) {
            unlink('Admin/files/Career/' . basename($data->image));
        }
        $data->delete();
        // Alert::success('Congrats', 'You\'ve Successfully Deleted Data');
        toast('Data Deleted!', 'success');
        return redirect()->route('admin-career.index');
    }
}
