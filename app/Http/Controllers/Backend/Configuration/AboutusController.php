<?php

namespace App\Http\Controllers\Backend\Configuration;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use File;
use Storage;
use Session;
use Alert;
use App\Models\Configuration\AboutUs;

class AboutusController extends Controller
{
    public function getConfiguration()
    {
        return view('dashboard.AboutUs.index');
    }
    public function postConfiguration(Request $request)
    {
        $inputs = $request->only(
            'first_title',
            'first_description',
            'first_title_np',
            'first_description_np',
            'image_one',
            'image_two',
            'image_three',

            'second_title',
            'second_description',
            'second_title_np',
            'second_description_np',
            'image_four',
            'image_five',
            'image_six',

            //about mission statement
            'mission_title',
            'mission_description',
            'mission_title_np',
            'mission_description_np',
        );

        foreach ($inputs as $inputKey => $inputValue) {
            if (
                $inputKey == 'image_one'
                || $inputKey == 'image_two'
                || $inputKey == 'image_three'
                || $inputKey == 'image_four'
                || $inputKey == 'image_five'
                || $inputKey == 'image_six'
            ) {
                $file = $inputValue;
                // Delete old file
                $this->deleteFile($inputKey);
                // Upload file & get store file name
                $inputValue   = $this->uploadFile($inputValue);
            }

            AboutUs::updateOrCreate(['configuration_key' => $inputKey], ['configuration_value' => $inputValue]);
        }
        // Alert::success('Congrats', 'You\'ve Successfully Updated Setting');
        toast('Data Updated!', 'success');
        return redirect()->back();
    }

    protected function uploadFile($file)
    {
        $image_new_name = time() . $file->getClientOriginalName();
        $file->move('Admin/images/AboutUs', $image_new_name);
        return 'Admin/images/AboutUs/' . $image_new_name;
    }

    protected function deleteFile($inputKey)
    {
        $configuration = AboutUs::where('configuration_key', '=', $inputKey)->first();
        // Check if configuration exists
        if (null !== $configuration && $configuration->exists()) {
            if (file_exists('Admin/images/AboutUs/' . basename($configuration->configuration_value))) {
                file::delete('Admin/images/AboutUs/' . basename($configuration->configuration_value));
            }
        }
    }
}
