<?php

namespace App\Http\Controllers\Backend\Configuration;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use File;
use Storage;
use Session;
use Alert;
use App\Models\Configuration\Configuration;

class ConfigurationController extends Controller
{
    public function getConfiguration(){
        return view('dashboard.Setting.index');
   }

   public function postConfiguration( Request $request ) {
    $inputs = $request->only(
        'map',
        'copyright',

        'logo',
        'fab_icon',
        'footer_logo',
        'nav_title',
        'fab_title',

        'contact_address',
        'contact_email_primary',
        'contact_phone_primary',
        'fax',
        'contact_email_secondary',
        'contact_phone_secondary',

        'contact_address_np',
        'contact_email_primary_np',
        'contact_phone_primary_np',
        'fax_np',
        'contact_email_secondary_np',
        'contact_phone_secondary_np',
    );

    foreach ($inputs as $inputKey => $inputValue) {
        if ($inputKey == 'logo'
        || $inputKey == 'fab_icon'
        || $inputKey == 'footer_logo') {
            $file = $inputValue;
            // Delete old file
            $this->deleteFile($inputKey);
            // Upload file & get store file name
            $inputValue   = $this->uploadFile($inputValue);
        }

        Configuration::updateOrCreate(['configuration_key' => $inputKey], ['configuration_value' => $inputValue]);
    }
    // Alert::success('Congrats', 'You\'ve Successfully Updated Setting');
    toast('Data Updated!', 'success');
    return redirect()->back();
}

protected function uploadFile($file)
{
    $image_new_name = time() . $file->getClientOriginalName();
    $file->move('Admin/images/Configurations', $image_new_name);
    return 'Admin/images/Configurations/' . $image_new_name;
}

protected function deleteFile($inputKey)
{
    $configuration = Configuration::where('configuration_key', '=', $inputKey)->first();
    // Check if configuration exists
    if (null !== $configuration && $configuration->exists()) {
        if (file_exists('Admin/images/Configurations/' . basename($configuration->configuration_value))) {
            file::delete('Admin/images/Configurations/' . basename($configuration->configuration_value));
        }
    }
}
}
