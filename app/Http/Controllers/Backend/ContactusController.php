<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\ContactUs\ContactUsDetail;
use Illuminate\Http\Request;
use App\Services\GenericService;
use Yajra\DataTables\DataTables;

class ContactusController extends Controller
{
    protected $genericService;

    public function __construct(GenericService $genericService)
    {
        $this->genericService = $genericService;
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = ContactUsDetail::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    $actionBtn = '<a title="Edit About-Us" href="/admin-contact-detail/' . $data->id . '/edit" class="btn btn-sm btn-primary mr-2"><i class="fas fa-edit"></i></a>';
                    $actionBtn .= '<a title="Delete About-Us" class="btn btn-sm btn-danger" onclick="handleDelete(' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                    return $actionBtn;
                })
                ->addColumn('status', function ($data) {
                    return $data->status == '0' ? 'In-Active' : 'Active';
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('dashboard.contact_us.contact_us_detail.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('dashboard.contact_us.contact_us_detail.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'location' => 'required',
            'location_np' => 'required',
        ]);

        $this->genericService->store(new ContactUsDetail, $request->only(['location','location_np','phone','phone_np','status']));
        toast('Data Stored!', 'success');
        return redirect()->route('admin-contact-detail.index')->with('success', 'Data created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = ContactUsDetail::findOrFail($id);
        return view('dashboard.contact_us.contact_us_detail.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $validatedData = $request->validate([
            'location' => 'required',
            'location_np' => 'required',
        ]);

        $result = $this->genericService->update(new ContactUsDetail, $request->only(['location','location_np','phone','phone_np','status']), $id);

        if ($result) {
            toast('Data Updated!', 'success');
            return redirect()->route('admin-contact-detail.index')->with('success', 'Data updated successfully.');
        } else {
            return redirect()->route('admin-contact-detail.index')->with('error', 'Failed to update testimonial.');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = ContactUsDetail::find($id);
        $data->delete();
        toast('Data Deleted!', 'success');
        // Alert::success('Congrats', 'You\'ve Successfully Deleted Data');
        return redirect()->route('admin-contact-detail.index');
    }
}
