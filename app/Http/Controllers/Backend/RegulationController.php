<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Regulation\Regulation;
use App\Models\Regulation\RegulationCategory;
use Illuminate\Http\Request;
use App\Services\GenericService;
use Yajra\DataTables\DataTables;

class RegulationController extends Controller
{
    protected $genericService;

    public function __construct(GenericService $genericService)
    {
        $this->genericService = $genericService;
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Regulation::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    $actionBtn = '<a title="Edit About-Us" href="/admin-regulation/' . $data->id . '/edit" class="btn btn-sm btn-primary mr-2"><i class="fas fa-edit"></i></a>';
                    $actionBtn .= '<a title="Delete About-Us" class="btn btn-sm btn-danger" onclick="handleDelete(' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                    return $actionBtn;
                })
                ->addColumn('status', function ($data) {
                    return $data->status == '0' ? 'In-Active' : 'Active';
                })
                ->addColumn('image', function ($data) {
                    $imagePath = $data->image ? asset('Admin/files/Regulation/'.$data->image) : asset('default_image/profile.png');
                    return '<a href="'.$imagePath.'" width="50" height="50" target="__blank" />View</a>';
                })
                ->rawColumns(['action','image'])
                ->make(true);
        }
        return view('dashboard.regulation.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $category=RegulationCategory::all();
        return view('dashboard.regulation.create',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required',
            'title_np' => 'required',
        ]);

        $this->genericService->store(new Regulation, $request->only(['title','title_np','description','description_np','slug','status','category_id','type']), $request->file('image'), 'image', 'title','type');
        toast('Data Stored!', 'success');
        return redirect()->route('admin-regulation.index')->with('success', 'Data created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = Regulation::findOrFail($id);
        $category=RegulationCategory::all();
        return view('dashboard.regulation.edit', compact('data','category'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $validatedData = $request->validate([
            'title' => 'required',
            'title_np' => 'required',
        ]);

        $result = $this->genericService->update(new Regulation, $request->only(['title','title_np','description','description_np','slug','status','category_id','type']), $id, $request->file('image'), 'image', 'title', 'Admin/files/Regulation');

        if ($result) {
            toast('Data Updated!', 'success');
            return redirect()->route('admin-regulation.index')->with('success', 'Data updated successfully.');
        } else {
            return redirect()->route('admin-regulation.index')->with('error', 'Failed to update testimonial.');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = Regulation::find($id);
        if (file_exists('Admin/files/Regulation/' . basename($data->image))) {
            unlink('Admin/files/Regulation/' . basename($data->image));
        }
        $data->delete();
        // Alert::success('Congrats', 'You\'ve Successfully Deleted Data');
        toast('Data Deleted!', 'success');
        return redirect()->route('admin-regulation.index');
    }
}
