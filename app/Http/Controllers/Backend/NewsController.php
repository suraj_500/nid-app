<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HomePage\News;
use App\Services\GenericService;
use Yajra\DataTables\DataTables;

class NewsController extends Controller
{
    protected $genericService;

    public function __construct(GenericService $genericService)
    {
        $this->genericService = $genericService;
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = News::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    $actionBtn = '<a title="Edit About-Us" href="/admin-news/' . $data->id . '/edit" class="btn btn-sm btn-primary mr-2"><i class="fas fa-edit"></i></a>';
                    $actionBtn .= '<a title="Delete About-Us" class="btn btn-sm btn-danger" onclick="handleDelete(' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                    return $actionBtn;
                })
                ->addColumn('status', function ($data) {
                    return $data->status == '0' ? 'In-Active' : 'Active';
                })
                ->addColumn('image', function ($data) {
                    $imagePath = $data->image ? asset('Admin/files/News/'.$data->image) : asset('default_image/profile.png');
                    return '<img src="'.$imagePath.'" width="50" height="50" />';
                })
                ->rawColumns(['action','image'])
                ->make(true);
        }
        return view('dashboard.homepage.news.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('dashboard.homepage.news.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required',
            'title_np' => 'required',
        ]);

        $this->genericService->store(new News, $request->only(['title','title_np','description','description_np','slug','published_date','author','author_np','status']), $request->file('image'), 'image', 'slug');
        toast('Data Stored!', 'success');
        return redirect()->route('admin-news.index')->with('success', 'Data created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = News::findOrFail($id);
        return view('dashboard.homepage.news.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $validatedData = $request->validate([
            'title' => 'required',
            'title_np' => 'required',
        ]);

        $result = $this->genericService->update(new News, $request->only(['title','title_np','description','description_np','slug','published_date','author','author_np','status']), $id, $request->file('image'), 'image', 'title', 'Admin/files/News');

        if ($result) {
            toast('Data Updated!', 'success');
            return redirect()->route('admin-news.index')->with('success', 'Data updated successfully.');
        } else {
            return redirect()->route('admin-news.index')->with('error', 'Failed to update testimonial.');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = News::find($id);
        if (file_exists('Admin/files/News/' . basename($data->image))) {
            unlink('Admin/files/News/' . basename($data->image));
        }
        $data->delete();
        // Alert::success('Congrats', 'You\'ve Successfully Deleted Data');
        toast('Data Deleted!', 'success');
        return redirect()->route('admin-news.index');
    }
}
