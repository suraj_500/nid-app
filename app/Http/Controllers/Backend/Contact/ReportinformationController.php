<?php

namespace App\Http\Controllers\Backend\Contact;

use App\Http\Controllers\Controller;
use App\Models\ReportInformation\ReportInformation;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ReportinformationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = ReportInformation::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    $actionBtn = '<a title="Delete About-Us" class="btn btn-sm btn-danger" onclick="handleDelete(' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                    return $actionBtn;
                })
                ->addColumn('file', function ($data) {
                    $imagePath = $data->file ? asset('Admin/files/ReportInformation/'.$data->file) : asset('default_image/profile.png');
                    return '<a href="'.$imagePath.'" width="50" height="50" target="__blank" />View</a>';
                })
                ->rawColumns(['action','file'])
                ->make(true);
        }
        return view('dashboard.contact_us.report_information.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = ReportInformation::find($id);
        if (file_exists('Admin/files/ReportInformation/' . basename($data->file))) {
            unlink('Admin/files/ReportInformation/' . basename($data->file));
        }
        $data->delete();
        // Alert::success('Congrats', 'You\'ve Successfully Deleted Data');
        toast('Data Deleted!', 'success');
        return redirect()->route('admin-report-information.index');
    }
}
