<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Gallery\Gallery;
use App\Models\Gallery\GalleryImage;
use App\Services\GalleryService;
use Yajra\DataTables\DataTables;
use File;

class GalleryController extends Controller
{
    protected $galleryService;

    public function __construct(GalleryService $galleryService)
    {
        $this->galleryService = $galleryService;
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Gallery::with('galleryImages')->latest()->get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    $editUrl = route('admin-gallery.edit', $data->id);
                    $actionBtn = '<a title="Edit Gallery" href="' . $editUrl . '" class="btn btn-sm btn-primary mr-2"><i class="fas fa-edit"></i></a>';
                    $actionBtn .= '<a title="Delete Gallery" class="btn btn-sm btn-danger" onclick="handleDelete(' . $data->id . ')"><i class="fas fa-trash"></i></a>';
                    return $actionBtn;
                })
                ->addColumn('status', function ($data) {
                    return $data->status == '0' ? 'In-Active' : 'Active';
                })
                ->addColumn('view', function ($data) {
                    $viewButton = $data->galleryImages->count() > 0 ?
                        '<a class="link m-2" data-toggle="modal" data-target="#previewGalleryImages' . $data->id . '">View images</a>' :
                        'No images';
                    return $viewButton;
                })
                ->rawColumns(['action', 'status', 'view'])
                ->make(true);
        }

        $galleries = Gallery::with('galleryImages')->latest()->get();
        return view('dashboard.gallery.index', compact('galleries'));
    }



    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('dashboard.gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['image'] = $request->file('image');

        $this->galleryService->store($data);

        return redirect()->route('admin-gallery.index')->with('success', 'Gallery created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = Gallery::findOrFail($id);
        return view('dashboard.gallery.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $data = $request->all();
        $data['image'] = $request->file('image');

        $this->galleryService->update($data, $id);

        return redirect()->route('admin-gallery.index')->with('success', 'Gallery updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $gallery = Gallery::where('id', $id)->firstOrFail();

        $destination = 'Admin/files/Gallery/' . $gallery->featured_image;
        if (file::exists($destination)) {
            file::delete($destination);
        }

        $gallery->deleteImage();
        foreach($gallery->galleryimages as $galleryimage) {
            $galleryimage->deleteImage();
            $galleryimage->delete();
        }
        $gallery->delete();
        return redirect()->back()->with('success','Deleted Successfully');
    }

    public function removeGalleryImage($id)
    {
        $galleryImage = GalleryImage::findOrFail($id);
        $galleryImage->deleteImage();
        $galleryImage->delete();
        return redirect()->back()->with('success', 'Deleted Successfully');
    }
}
