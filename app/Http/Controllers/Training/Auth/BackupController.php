<?php

namespace App\Http\Controllers\Training\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Training\TrainingStaff;
use Auth;
use Session;
use Carbon\Carbon;
use Hash;

class LoginController extends Controller
{
    public function showEmailForm()
    {
        return view('Training.auth.login_testone');
    }

    public function submitEmail(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
        ]);

        // Store the email in the session
        Session::put('email', $request->email);

        return redirect()->route('training.login.password');
    }

    public function showPasswordForm()
    {
        $email = Session::get('email');

        if (!$email) {
            return redirect()->route('training.login.email');
        }

        return view('Training.auth.login_testtwo', ['email' => $email]);
    }

    public function submitPassword(Request $request)
    {
        $request->validate([
            'password' => 'required',
        ]);

        $credentials = [
            'email' => Session::get('email'),
            'password' => $request->password
        ];

        // Retrieve the user based on the email
        $user = TrainingStaff::where('email', $credentials['email'])->first();

        if ($user && Hash::check($credentials['password'], $user->password)) {
            // Check if the expiry date is not exceeded or is null
            if (is_null($user->expiry_date) || Carbon::now()->lessThanOrEqualTo(Carbon::parse($user->expiry_date))) {
                // Log the user in
                Auth::guard('training')->login($user);
                return redirect()->route('training-dashboard.index');
            } else {
                // Expiry date exceeded
                return redirect()->back()->with('errormessage', 'Your account has expired!');
            }
        } else {
            // Authentication failed
            return redirect()->back()->with('errormessage', 'Invalid credentials!');
        }
    }

    public function trainingLogout()
    {
        Auth::guard('training')->logout();
        Session::flash('info_message', 'Logout Succesfully');
        return redirect()->route('training.login.email');
    }
}
