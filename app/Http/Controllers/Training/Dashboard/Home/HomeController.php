<?php

namespace App\Http\Controllers\Training\Dashboard\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        return view('Training.dashboard.index');
    }
}
