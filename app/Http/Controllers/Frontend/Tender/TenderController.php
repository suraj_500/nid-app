<?php

namespace App\Http\Controllers\Frontend\Tender;

use App\Http\Controllers\Controller;
use App\Models\Tender\Tender;
use Illuminate\Http\Request;

class TenderController extends Controller
{
    public function index(){
        $tender=Tender::orderby('id', 'desc')->paginate(2);
        // $tender=Tender::orderby('id', 'desc')->get();
        return view('Frontend.Tender.index',compact('tender'));
    }
}
