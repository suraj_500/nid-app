<?php

namespace App\Http\Controllers\Frontend\Regulation;

use App\Http\Controllers\Controller;
use App\Models\Regulation\Regulation;
use App\Models\Regulation\RegulationCategory;
use Illuminate\Http\Request;

class RegulationController extends Controller
{
    public function index(){
        $regulation_categories=RegulationCategory::with('regulations')->get();
        $regulation=Regulation::orderby('id', 'desc')->paginate(10);
        return view('Frontend.Regulation.index',compact('regulation_categories','regulation'));
    }
}
