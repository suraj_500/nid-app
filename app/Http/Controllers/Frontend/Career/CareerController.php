<?php

namespace App\Http\Controllers\Frontend\Career;

use App\Http\Controllers\Controller;
use App\Models\Career\Career;
use App\Models\Career\CareerCategory;
use Illuminate\Http\Request;

class CareerController extends Controller
{
    public function index(){
        $notice_categories=CareerCategory::with('careers')->get();
        $notice=Career::orderby('id', 'desc')->paginate(10);
        return view('Frontend.Career.index',compact('notice_categories','notice'));
    }
}
