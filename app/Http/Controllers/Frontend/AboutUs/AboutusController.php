<?php

namespace App\Http\Controllers\Frontend\AboutUs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AboutusController extends Controller
{
    public function index(){
        return view('Frontend.AboutUs.index');
    }
}
