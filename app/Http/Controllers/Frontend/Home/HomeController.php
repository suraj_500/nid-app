<?php

namespace App\Http\Controllers\Frontend\Home;

use App\Http\Controllers\Controller;
use App\Models\HomePage\Banner;
use App\Models\HomePage\BannerNotice;
use App\Models\HomePage\News;
use App\Models\HomePage\Notice;
use App\Models\HomePage\NoticeCategory;
use App\Models\Staff\Staff;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        // $notice_categories = NoticeCategory::with('notices')->get();
        // $notice = Notice::orderby('id', 'desc')->paginate(10);
        $notice_categories = NoticeCategory::all();
        $notice_categories->each(function($category) {
            $category->notices = Notice::where('category_id', $category->id)->paginate(10);
        });
        $news = News::orderby('id', 'desc')->get();
        $banner = Banner::orderby('id', 'desc')->get();
        $staff=Staff::orderby('id','desc')->limit(2)->get();
        return view('Frontend.index', compact('notice_categories', 'news', 'banner','staff'));
    }

    public function getNewsdetail($slug)
    {
        $news_detail = News::where('slug', $slug)->firstOrFail();
        return view('Frontend.home_section.news_detail',compact('news_detail'));
    }
}
