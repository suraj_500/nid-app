<?php

namespace App\Http\Controllers\Frontend\Notice;

use App\Http\Controllers\Controller;
use App\Models\HomePage\Notice;
use App\Models\HomePage\NoticeCategory;
use Illuminate\Http\Request;

class NoticeController extends Controller
{
    public function index() {
        $notice_categories = NoticeCategory::all();
        $notice_categories->each(function($category) {
            $category->notices = Notice::where('category_id', $category->id)->paginate(1);
        });
        return view('Frontend.Notice.index', compact('notice_categories'));
    }

}
