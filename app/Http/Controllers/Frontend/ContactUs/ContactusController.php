<?php

namespace App\Http\Controllers\Frontend\ContactUs;

use App\Http\Controllers\Controller;
use App\Models\ContactUs\ContactUs;
use App\Models\ContactUs\ContactUsDetail;
use Illuminate\Http\Request;

class ContactusController extends Controller
{
    public function index(){
        $contact_detail=ContactUsDetail::orderby('id', 'desc')->limit('7')->get();
        return view('Frontend.ContactUs.index',compact('contact_detail'));
    }

    public function postIndex(Request $request){
        $this->validate($request, [
            'full_name' => 'required',
            'phone' => 'required',
            'file' => 'required',
            'subject' => 'required',
        ]);

        $input = $request->all();
        $contact = ContactUs::create($input);
        // Mail::to('info@nia.gov.np')->send(new ContactMail($input));
        // Alert::success('Congrats', 'Message Sent Successfully');
        return redirect()->route('contact-us');
    }
}
