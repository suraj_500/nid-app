<?php

namespace App\Http\Controllers\Frontend\ReportInformation;

use App\Http\Controllers\Controller;
use App\Models\ReportInformation\ReportInformation;
use Illuminate\Http\Request;
use App\Services\GenericService;


class ReportInformationController extends Controller
{
    protected $genericService;

    public function __construct(GenericService $genericService)
    {
        $this->genericService = $genericService;
    }
    public function index()
    {
        return view('Frontend.ReportInformation.index');
    }

    public function postIndex(Request $request){
        // $validatedData = $request->validate([
        //     'title' => 'required',
        //     'title_np' => 'required',
        // ]);

        $this->genericService->store(new ReportInformation, $request->only(['full_name','phone','email','subject']), $request->file('file'), 'file');
        toast('Data Stored!', 'success');
        return redirect()->route('report-information')->with('success', 'Data created successfully.');
    }
}
