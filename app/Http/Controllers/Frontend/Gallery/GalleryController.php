<?php

namespace App\Http\Controllers\Frontend\Gallery;

use App\Http\Controllers\Controller;
use App\Models\Gallery\Gallery;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    public function index(){
        $galleries = Gallery::orderby('id', 'desc')->with('galleryImages')->get();
        return view('Frontend.Gallery.index', ['galleries' => $galleries]);
    }
}
